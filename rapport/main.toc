\babel@toc {french}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{3}{}%
\contentsline {section}{\numberline {2}Contexte}{4}{}%
\contentsline {subsection}{\numberline {2.1}Analyse}{4}{}%
\contentsline {subsection}{\numberline {2.2}Méthode agile}{5}{}%
\contentsline {subsubsection}{\numberline {2.2.1}Définition}{5}{}%
\contentsline {subsubsection}{\numberline {2.2.2}La méthode Scrum}{6}{}%
\contentsline {subsubsection}{\numberline {2.2.3}Mise en place concrète}{6}{}%
\contentsline {subsubsection}{\numberline {2.2.4}User stories}{7}{}%
\contentsline {subsection}{\numberline {2.3}Langages, technologies et outils}{8}{}%
\contentsline {subsubsection}{\numberline {2.3.1}L'environnement de Blitz}{8}{}%
\contentsline {subsubsection}{\numberline {2.3.2}TypeScript}{9}{}%
\contentsline {subsubsection}{\numberline {2.3.3}React}{10}{}%
\contentsline {subsubsection}{\numberline {2.3.4}Next.js}{11}{}%
\contentsline {subsubsection}{\numberline {2.3.5}Prisma}{13}{}%
\contentsline {subsubsection}{\numberline {2.3.6}Docker}{14}{}%
\contentsline {subsubsection}{\numberline {2.3.7}Blitz.js}{15}{}%
\contentsline {subsubsection}{\numberline {2.3.8}Les librairies tiers}{15}{}%
\contentsline {subsubsection}{\numberline {2.3.9}AzureDevops}{16}{}%
\contentsline {subsubsection}{\numberline {2.3.10}Vim}{16}{}%
\contentsline {subsubsection}{\numberline {2.3.11}Git, GitLab et Fork}{16}{}%
\contentsline {section}{\numberline {3}L'application}{18}{}%
\contentsline {subsection}{\numberline {3.1}Présentation de l'application}{18}{}%
\contentsline {subsection}{\numberline {3.2}Difficultés rencontrées, modifications des plans}{19}{}%
\contentsline {subsubsection}{\numberline {3.2.1}La cogestion de la conception}{19}{}%
\contentsline {subsubsection}{\numberline {3.2.2}L'apprentissage de la technologie}{19}{}%
\contentsline {subsubsection}{\numberline {3.2.3}L'échelle de l'application}{20}{}%
\contentsline {subsubsection}{\numberline {3.2.4}L'évolution du schéma de la base de données}{21}{}%
\contentsline {subsubsection}{\numberline {3.2.5}La génération de PDF}{21}{}%
\contentsline {subsubsection}{\numberline {3.2.6}La gestion des \textit {components}}{21}{}%
\contentsline {subsubsection}{\numberline {3.2.7}La modification du premier public cible}{22}{}%
\contentsline {subsection}{\numberline {3.3}Les limites du projet}{22}{}%
\contentsline {subsubsection}{\numberline {3.3.1}La gestion du \textit {responsive}}{22}{}%
\contentsline {subsubsection}{\numberline {3.3.2}L'environnement de production}{23}{}%
\contentsline {subsubsection}{\numberline {3.3.3}Le système de monétisation}{23}{}%
\contentsline {subsection}{\numberline {3.4}L'application aujourd'hui et le MVP}{23}{}%
\contentsline {subsubsection}{\numberline {3.4.1}La gestion de compte utilisateur}{23}{}%
\contentsline {subsubsection}{\numberline {3.4.2}Le coeur de l'application}{24}{}%
\contentsline {subsubsection}{\numberline {3.4.3}Le produit de l'application}{26}{}%
\contentsline {subsection}{\numberline {3.5}L'avenir du projet}{26}{}%
\contentsline {section}{\numberline {4}Conclusion}{27}{}%
\contentsline {section}{\numberline {5}Bibliographie}{28}{}%
\contentsline {section}{\numberline {6}Annexes}{29}{}%
\contentsline {subsection}{\numberline {6.1}Annexe 1: Exepmple de tableau de services d'un électricien}{29}{}%
\contentsline {subsection}{\numberline {6.2}Annexe 2: Schéma de la base de données}{30}{}%
