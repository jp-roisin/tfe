User Stories:


Outils, technologies et langages:

BlitzJs est un framework fullstack. Sa construction a été inspirée par Ruby on Rails. Basé sur NextJs, cet outil apporte une surcouche qui lie l’application à une base de donnée. Pour faciliter les échanges de données, BlitzJs propose une approche Zero-API, ce qui élimine l'utilisation d'API Rest ou GraphQl. BlitzJs englobent plusieurs technologies en un seul framework. Intéressons-nous à ces outils: 

IMG
(schema TypeScript > blitz > (prisma > docker) + (next > react))

TypeScript:
L'ensemble de ce travail a été réalisé en TypeScript. C'est un surlangage de JavaScript, qui permet un typage stricte des variables. Il apporte un ensemble d'outils facilitant le travail de développement et la croissance de l'application. 
Même si son apprentissage m'a ralenti durant les premières semaines de développement, il s'est avéré que son utilisation ma fait gagner du temps sur le long tèrme. 
Par expemple, lorsque l'on modifie le typage d'une variable en TypeScript, c'est l'éditeur de code qui va nous indiquer toutes les erreurs qui vont surgir à la suite de cette modification. On peut donc les corriger avant même de lancer l'application. C'est non seulement un gain de temps, mais c'est surtout un gain de confort pour le développeur car il n'a pas a constament tester la fonctionalité qu'il est en train de mettre en place.
TypeScript est donc un outil qui facilite le développement au seul coup d'une syntaxe alourdie.  


React:
React permet de diviser l'application en sous éléments que l'on peut faire interragir en utilisant du JavaScript. Les avantages sont que l'on peut ainsi réutiliser ces fragments sans dupliquer leur code, les appeler conditionnelement et les faire varier celon le contexte. React est un outil formidable pour dynamiser un site internet en le rendant monopage (Single Page Application). 
Chaque compomenents est une fonction qui retourne du JSX (JavaScript XML). L'élément est donc une balise personalisée et celle-ci peut en appeler d'autres pour modeler l'application. L'avantage de l'approche fonctionelle est la possiblité d'utiliser des paramètres. Dans l'environement de React, ils sont appeler Props et permettent aux compomenents de se passer des informations entre eux. 
React apporte également au développeur la possiblité de manipuler le Document Object Model (DOM) sans passer par ses propriétes et ses méthodes natives. C'est une amélioration de la qualité de développement car ces dernières sont redondantes et en s'accumulant, rendent rapidement le code illisible. A la place des méthodes du DOM, React utilse des états (State). Un state est une variable. A chaque modification de cette variable, le compomenents va être réafficher (rerender) acec la nouvelle information. Il n'y a donc pas de chargement de page à chaque modification de la variable ce qui rend l'application fluide.

CODE
(expemple incrémenteur)

NextJs:
NextJs est un framework basé sur React. Il en reprend toute l'architecture de compomenents mais y ajoute plusieurs fonctionalités. 
La plus importante est le SSR (Server Side Rendering) qui corrige un défaut de React. En effet, React utilse le CSR (Client Side Rendering) ce qui veut dire que c'est le navigateur de l'utilisateur qui exécute l'application. Cela implique plusieurs effets négatifs. L'expérience utilisateur va être moindre si l'appareil est plus lent. Mais surtout le site aura du mal à gagner en référencement car les programes de SEO (Search Engine Optimization) vont avoir plus de mal à analyser l'information qui est rendu conditionnelement selon les échanges avec l'utilisateur. En revanche, le Server Side Rendering génère côté serveur l'HTML sur base du JavaScript et l'envoie au client. Cela soulage la machine de l'utilisateur et permet aux programes de SEO une meilleure analyse et donc un meilleur référencement de l'application. 

IMG
(schema SSR vs CSR)
https://kruschecompany.com/ssr-or-csr-for-progressive-web-app/#:~:text=In%20CSR%2C%20the%20initial%20page,needed%20to%20generate%20the%20template.

Un autre avantage qu'apporte NextJs est son système de routage (Routing). Le routage c'est le méchanisme qui sélection les chemins pour guider les données dans le réseau. En d'autres tèrmes, c'est la méthode de gestion des URL (Uniform Resource Locator) de l'application. React ne propose aucun système de routage, et force donc les développeurs à passer par une librairie tiers comme React-Router. NextJs en revanche contient un systeme de routage basé sur les fichiers eux même. Si un fichier est ajouté dans le dossier "pages", son nom devient une route et son contenu devient la page en elle même. On peut aller encore plus loins en imbriquant plusieurs dossiers pour former une route emboîtée.

IMG
(exemple : https://my-app.be/dossier1/dossier2/fichier1)


Prisma
Prisma est un ORM (Object Relational Mapping) construit pour fonctionner avec JavaScript et TypeScript. Via son fichier schema.prisma, il permet de génèrer une base de données SQL (Structural Query Language) de façon lisible pour l'être humain. Prisma apporte aussi un ensemble d'outil de CLI (Command Line Inteface) pour gérer les migrations et le contenu de la base de donnée. De plus, il permet à l'application de récupérer le typage de chaque champs la DB (Data Base). Cela facilite le travail du développeur car il peut ainsi utiliser l'auto completion de son éditeur de code pour retrouver les différentes propriétés d'une table. On peut donc profiter de tous les intérets du TypeScript depuis la source même d'application. Chaque variable que l'on va vouloir sauver sur notre base de données devra être de même type que le champs de cette table. C'est l'éditeur de code qui nous fera remarquer si il y a une différence. Prisma permet également de choisir le type de base de données. On a la possiblité d'utiliser une DB relationelle ou MongoDB. Dans mon cas, j'ai choisi une MySQL (MariaDB). 


Docker:
La base de données de l'application est stocké dans un container Docker. Cela permet d'isoler ce service du reste de l'application afin qu'il fonctionne dans un environement constant. En effet, une fois l'application déployée, il suffit de migrer le container sur le serveur et ainsi, son environement n'aura pas changé. On ne soufrera pas des différence entre la machine de développement et le serveur de production.
Il n'y a actuelement qu'une seule image sur le Docker de l'application, celle de la base de données. Grace au fichier docker-compose.yaml, on peut facilitant ajouter d'autres images afin de greffer un nouveau service.

CODE
(docker-compose.yaml)


Blitzjs:
En plus de toutes ces technologies, BlitzJs propose une variété d'outils facilitant la mise en oeuvre d'une application.
Gestion des queries et mutation
Les Hooks


Librairies :

Zod:
Zod est une librairie de déclaration de schéma et de validation conçu pour le TypeScript. Son but est d'éliminer le dédoublement de déclaration de typage. 

CODE
(ex validation mutation)


Date-fns: 
Date-fns est une libraire minimaliste de gestion de date et de temps en JavaScript. Il fonctionne sur le navigateur et dans NodeJs. 

CODE
(ex fomat date)

Mui, React Final Form & Mui-rff
Mui, React Final Form et Mui-rff sont des librairies de gestion de formulaires. Mui propose une suite de compomenents de formulaires et d'autres éléments d'UI (User Inteface) communs dans les applications modernes. React Final Form lui s'occupe de gérer les états des différents champs d'un formulaire. Mui-rff permet d'utiliser conjointements les deux librairies précedemment évoquées. 


Type-fest: 
Type-fest est une libraire de typage qui permet d'utiliser des types plus complexes que ceux proposés nativement par TypeScript.

CODE 
(ex quote)

TailwindCss & ClassNames:
TailwindCss est un framework de CSS qui permet de styliser les balises sans passer par des feuilles de CSS qui ont tendance à s'allourdir au fur et à mesure que l'application grandi. En écrivant du CSS racourcis dirrectement dans la balise, on améliore la maintenance du code et sa lisibilité.
ClassNames permet de conditioner les propriétes de TailwindCss. 

CODE 
(ex tailwnd + classNames)


Uuid: 
Uuid est une librairie de génération sécurisées d'uuid (Universally Unique Identifier). Il fonctionne également sur le navigateur et dans Nodejs.  

CODE
(ex uuid)




proficient :
  - Pomodoro
  - Vim
  - AzureDevops
