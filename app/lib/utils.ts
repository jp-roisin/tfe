export const notUndefined = <T>(value: T | undefined) => {
  if (typeof value === "undefined") {
    throw new Error("Value is undefined")
  }
  return value
}
