import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { ActivationKeys } from "app/utilities/account/lib/ListActivationKeys"
import { NavSecond } from "app/core/layouts/NavSecond"

const Admin: BlitzPage = () => {
  return (
    <>
      <NavSecond>
        <ActivationKeys />
      </NavSecond>
    </>
  )
}

Admin.suppressFirstRenderFlicker = true
Admin.getLayout = (page) => <Layout title="Admin">{page}</Layout>

export default Admin
