import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { NavSecond } from "app/core/layouts/NavSecond"
import { LogoutButton } from "app/utilities/auth/components/LogoutButton"
import { Subtitle } from "app/core/components/Subtitile"

const Settings: BlitzPage = () => {
  return (
    <>
      <NavSecond>
        <div>
          <Subtitle>User settings</Subtitle>
          <LogoutButton />
        </div>
      </NavSecond>
    </>
  )
}

Settings.suppressFirstRenderFlicker = true
Settings.getLayout = (page) => <Layout title="Account">{page}</Layout>

export default Settings
