import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { UserUpdateForm } from "app/utilities/user/lib/UserUpdateForm"
import { NavSecond } from "app/core/layouts/NavSecond"

const Account: BlitzPage = () => {
  return (
    <>
      <NavSecond>
        <UserUpdateForm />
      </NavSecond>
    </>
  )
}

Account.suppressFirstRenderFlicker = true
Account.getLayout = (page) => <Layout title="Account">{page}</Layout>

export default Account
