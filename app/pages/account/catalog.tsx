import { BlitzPage, useMutation } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { NavSecond } from "app/core/layouts/NavSecond"
import deleteElectricItem from "app/utilities/electricItem/mutations/deleteElectricItem"
import { ListElectricItems } from "app/utilities/electricItem/lib/ListElectricItems"

const Catalog: BlitzPage = () => {
  return (
    <>
      <ElectricItems />
    </>
  )
}

const ElectricItems = () => {
  const [deleteElectricItemMutation] = useMutation(deleteElectricItem)

  const deleteServiceHandler = async (itemId: number) => {
    await deleteElectricItemMutation(itemId)
  }

  return (
    <>
      <NavSecond>
        <ListElectricItems button={{ label: "-", onClick: deleteServiceHandler }} />
      </NavSecond>
    </>
  )
}

Catalog.suppressFirstRenderFlicker = true
Catalog.getLayout = (page) => <Layout title="Account">{page}</Layout>

export default Catalog
