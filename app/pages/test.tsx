import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { CreateRoom } from "app/utilities/room/lib/CreateRoom"
import { ListRooms } from "app/utilities/room/lib/ListRooms"
import { Suspense } from "react"
import { AddElectricItemsToRoom } from "app/utilities/room/lib/AddElectricItemsToRoom"

const Test: BlitzPage = () => {
  return (
    <div>
      <Suspense fallback="Loading...">
        <ListRooms siteId={1} />
      </Suspense>
    </div>
  )
}

Test.suppressFirstRenderFlicker = true
Test.getLayout = (page) => <Layout title="home">{page}</Layout>

export default Test
