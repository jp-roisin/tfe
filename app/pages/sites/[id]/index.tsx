import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { Suspense } from "react"
import { SiteUpdate } from "app/utilities/site/lib/SiteUpdate"

const SitesIndex: BlitzPage = () => {
  return (
    <>
      <Suspense fallback="Loading ...">
        <SiteUpdate />
      </Suspense>
    </>
  )
}

SitesIndex.suppressFirstRenderFlicker = true
SitesIndex.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default SitesIndex
