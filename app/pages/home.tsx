import { LinkCard } from "app/core/components/LinkCard"
import { LayoutHome } from "app/core/layouts/LayoutHome"
import { BlitzPage } from "blitz"

const Menu = [
  {
    label: "Créer un devis",
    href: "/quotes",
  },
  {
    label: "Créer une facture",
    href: "#",
  },
  {
    label: "Compatbilité",
    href: "#",
  },
  {
    label: "Recherche par dossier",
    href: "#",
  },
  {
    label: "Trésorerie",
    href: "#",
  },
  {
    label: "Dashboard",
    href: "#",
  },
]

const Home: BlitzPage = () => {
  return (
    <div className="w-[80%] flex justify-between flex-wrap">
      {Menu.map(({ href, label }, i) => (
        <LinkCard key={i} href={href} label={label} />
      ))}
    </div>
  )
}

Home.suppressFirstRenderFlicker = true
Home.getLayout = (page) => <LayoutHome title="home">{page}</LayoutHome>

export default Home
