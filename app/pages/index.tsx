import { Suspense } from "react"
import { BlitzPage, useRouter, useSession } from "blitz"
import { LayoutBase } from "app/core/layouts/LayoutBase"
import LoginPage from "app/utilities/auth/pages/login"

const UserCheck = () => {
  const session = useSession()
  const router = useRouter()

  if (!session.userId) {
    router.push("/login")
    return (
      <>
        <h3>Unlogged</h3>
      </>
    )
  } else {
    router.push("/account")
    return <h3>Logged</h3>
  }
}

const Index: BlitzPage = () => {
  return (
    <Suspense fallback="Loading ... ">
      <UserCheck />
    </Suspense>
  )
}

Index.redirectAuthenticatedTo = "/home"
Index.suppressFirstRenderFlicker = true
Index.getLayout = (page) => <LayoutBase title="Home">{page}</LayoutBase>

export default Index
