import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { QuoteStepper } from "app/utilities/quote/lib/QuoteStepper"

const Quote: BlitzPage = () => {
  return (
    <>
      <QuoteStepper />
    </>
  )
}

Quote.suppressFirstRenderFlicker = true
Quote.getLayout = (page) => <Layout title="quote">{page}</Layout>

export default Quote
