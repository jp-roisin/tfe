import { BlitzPage, Link, Router, useMutation, useQuery, useRouter } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { Suspense } from "react"
import { CreateArrayOfServices } from "app/utilities/service/lib/CreateArrayOfServices"
import getServicesByQuoteId from "app/utilities/quote/queries/getServicesByQuoteId"
import getCustomerByQuoteId from "app/utilities/quote/queries/getCustomerByQuoteId"
import { Subtitle } from "app/core/components/Subtitile"
import { Button } from "@mui/material"
import updateQuote from "app/utilities/quote/mutations/updateQuote"
import deleteQuote from "app/utilities/quote/mutations/deleteQuote"

const UpdateQuote = () => {
  const { query } = useRouter()
  const router = useRouter()
  const [item] = useQuery(getServicesByQuoteId, { id: Number(query.id) })
  const [customer] = useQuery(getCustomerByQuoteId, { id: Number(query.id) })
  const [updateServicesInQuoteMutation] = useMutation(updateQuote)
  const [deleteQuoteMutation] = useMutation(deleteQuote)
  const filledValues = item?.services || []
  const redirect = `/customers/${customer?.customerId}/quotes` || "#"
  return (
    <>
      <div className="my-6 flex justify-between items-center">
        <Subtitle>Mise à jour</Subtitle>
        <div>
          <Link href={redirect}>
            <a>
              <Button variant="text">Retour</Button>
            </a>
          </Link>
        </div>
      </div>
      <CreateArrayOfServices
        prevValues={[...filledValues]}
        submitText="Enregistrer"
        onSubmit={async (values) => {
          try {
            await updateServicesInQuoteMutation({
              serviceIds: values.map((s) => s.id),
              id: Number(query.id),
            })
            await router.push(redirect)
          } catch (error) {
            console.log(error)
          }
        }}
        deleteButton={{
          label: "Supprimer",
          onClick: async () => {
            console.log("click")
            try {
              await deleteQuoteMutation(Number(query.id))
              await router.push(redirect)
            } catch (error) {
              console.log(error)
            }
          },
        }}
      />
    </>
  )
}

const QuotesIndex: BlitzPage = () => {
  return (
    <>
      <Suspense fallback="Loading ...">
        <UpdateQuote />
      </Suspense>
    </>
  )
}

QuotesIndex.suppressFirstRenderFlicker = true
QuotesIndex.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default QuotesIndex
