import { BlitzPage, useRouter } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { NavSecond } from "app/core/layouts/NavSecond"
import { Suspense } from "react"

const CustomerInvoices: BlitzPage = () => {
  const { query } = useRouter()
  return (
    <Suspense fallback="Loading ...">
      <NavSecond>
        <p>Factures {query.id}</p>
      </NavSecond>
    </Suspense>
  )
}

CustomerInvoices.suppressFirstRenderFlicker = true
CustomerInvoices.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default CustomerInvoices
