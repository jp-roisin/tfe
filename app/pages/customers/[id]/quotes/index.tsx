import { BlitzPage, Link, useRouter } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { Suspense } from "react"
import { NavSecond } from "app/core/layouts/NavSecond"
import { MyButton } from "app/core/components/Button"
import { QuotesList } from "app/utilities/quote/lib/QuotesList"

const QuoteIndex: BlitzPage = () => {
  const { query } = useRouter()
  return (
    <>
      <Suspense fallback="Loading ...">
        <NavSecond>
          <>
            <div className="mb-6">
              <Link href={`/quotes`}>
                <a>
                  <MyButton variant="contained" shade="secondary">
                    Nouveau devis
                  </MyButton>
                </a>
              </Link>
            </div>
            <QuotesList customerId={Number(query.id)} complete />
          </>
        </NavSecond>
      </Suspense>
    </>
  )
}

QuoteIndex.suppressFirstRenderFlicker = true
QuoteIndex.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default QuoteIndex
