import { BlitzPage, Link, useMutation, useRouter } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { Button } from "@mui/material"
import { Suspense } from "react"
import createSite from "app/utilities/site/mutations/createSite"
import { CreateSite } from "app/utilities/site/lib/CreateSite"

const AddQuote = () => {
  const router = useRouter()
  const { query } = useRouter()
  const [createSiteMutation] = useMutation(createSite)
  return (
    <>
      <div className="mt-24">
        <CreateSite
          onSubmit={async (values) => {
            await createSiteMutation({ ...values, customerId: Number(query.id) })
            await router.push(`/customers/${Number(query.id)}/sites`)
          }}
          submitText="Créer"
        >
          <Link href={`/customers/${Number(query.id)}/sites`}>
            <a>
              <Button variant="text">Retour</Button>
            </a>
          </Link>
        </CreateSite>
      </div>
    </>
  )
}

const NewQuote: BlitzPage = () => {
  return (
    <>
      <Suspense fallback="Loading ...">
        <AddQuote />
      </Suspense>
    </>
  )
}

NewQuote.suppressFirstRenderFlicker = true
NewQuote.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default NewQuote
