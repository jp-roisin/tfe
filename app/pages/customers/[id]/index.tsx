import { BlitzPage } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { NavSecond } from "app/core/layouts/NavSecond"
import { CustomerUpdate } from "app/utilities/customer/lib/CustomerUpdate"
import { Suspense } from "react"

const CustomerIndex: BlitzPage = () => {
  return (
    <Suspense fallback="Loading ...">
      <NavSecond>
        <CustomerUpdate />
      </NavSecond>
    </Suspense>
  )
}

CustomerIndex.suppressFirstRenderFlicker = true
CustomerIndex.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default CustomerIndex
