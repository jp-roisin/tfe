import { BlitzPage, Link, useRouter } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { NavSecond } from "app/core/layouts/NavSecond"
import { Suspense } from "react"
import { ListCustomerSites } from "app/utilities/site/lib/ListCustomerSites"
import { MyButton } from "app/core/components/Button"

const CustomerSites: BlitzPage = () => {
  const { query } = useRouter()

  return (
    <>
      <Suspense fallback="Loading ...">
        <NavSecond>
          <>
            <div className="mb-6">
              <Link href={`/customers/${Number(query.id)}/sites/new`}>
                <a>
                  <MyButton variant="contained" shade="secondary">
                    Nouveau chantier
                  </MyButton>
                </a>
              </Link>
            </div>
            <ListCustomerSites customerId={Number(query.id)} />
          </>
        </NavSecond>
      </Suspense>
    </>
  )
}

CustomerSites.suppressFirstRenderFlicker = true
CustomerSites.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default CustomerSites
