import { BlitzPage, Link } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { Suspense } from "react"
import { MyButton } from "app/core/components/Button"
import { CustomersList } from "app/utilities/customer/lib/CustomersList"

const Customers: BlitzPage = () => {
  return (
    <>
      <div className="mt-24">
        <div className="mb-12">
          <Link href="/customers/new">
            <a>
              <MyButton variant="contained" shade="secondary">
                Nouveau client
              </MyButton>
            </a>
          </Link>
        </div>
        <Suspense fallback="Loading ...">
          <CustomersList />
        </Suspense>
      </div>
    </>
  )
}

Customers.suppressFirstRenderFlicker = true
Customers.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default Customers
