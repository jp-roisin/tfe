import { BlitzPage, Link, useMutation, useRouter } from "blitz"
import { Layout } from "app/core/layouts/Layout"
import { CreateCustomer } from "app/utilities/customer/lib/CreateCustomer"
import { Button } from "@mui/material"
import { Suspense } from "react"
import createCustomer from "app/utilities/customer/mutations/createCustomer"

const AddCustomer = () => {
  const router = useRouter()
  const [createCustomerMutation] = useMutation(createCustomer)
  return (
    <>
      <CreateCustomer
        onSubmit={async (values) => {
          await createCustomerMutation(values)
          await router.push("/customers")
        }}
        submitText="Créer"
      >
        <Link href="/customers">
          <a>
            <Button variant="text">Retour</Button>
          </a>
        </Link>
      </CreateCustomer>
    </>
  )
}

const NewCustomer: BlitzPage = () => {
  return (
    <>
      <Suspense fallback="Loading ...">
        <AddCustomer />
      </Suspense>
    </>
  )
}

NewCustomer.suppressFirstRenderFlicker = true
NewCustomer.getLayout = (page) => <Layout title="Customers">{page}</Layout>

export default NewCustomer
