import { MyButton } from "app/core/components/Button"
import Form from "app/core/components/Form"
import { useMutation } from "blitz"
import { TextField } from "mui-rff"
import createElectricItem, { CreateElectricItemValidation } from "../mutations/createElectricItem"

export const CreateElectricItem = ({ refreshList }: { refreshList: () => void }) => {
  const [createElectricItemMutation] = useMutation(createElectricItem)

  return (
    <>
      <Form
        onSubmit={async (values) => {
          try {
            await createElectricItemMutation(values)
            refreshList()
          } catch (error) {
            console.log(error)
          }
        }}
        schema={CreateElectricItemValidation}
      >
        <div className="flex">
          <div className="flex-1 mr-4">
            <TextField required size="small" name="name" label="Nouveau produit" fullWidth />
          </div>
          <div className="flex-3 mr-4">
            <TextField required size="small" name="price" label="Prix hTVA" fullWidth />
          </div>
          <MyButton
            variant="contained"
            shade="secondary"
            button={{ size: "small", type: "submit" }}
          >
            Ajouter
          </MyButton>
        </div>
      </Form>
    </>
  )
}
