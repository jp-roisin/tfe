import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { useQuery } from "blitz"
import { IconButton } from "@mui/material"
import { Subtitle } from "app/core/components/Subtitile"
import { Service } from "db"
import getUserElectricItems from "../queries/getUserElectricItems"
import { CreateElectricItem } from "./CreateElectricItem"

export const ListElectricItems = ({
  button,
}: {
  button: {
    onClick: (itemId: number) => void
    label: string | React.ReactNode
  }
}) => {
  const [userElectricItems, extras] = useQuery(getUserElectricItems, {})

  const refreshListHandler = () => {
    extras.refetch()
  }
  return (
    <>
      <Subtitle>Catalogue</Subtitle>
      <div className="my-8">
        <CreateElectricItem refreshList={refreshListHandler} />
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 600 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Produit</TableCell>
              <TableCell align="left">Prix hTVA</TableCell>
              <TableCell align="left"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userElectricItems.map(({ id, name, price }) => (
              <TableRow key={id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <TableCell align="left">{name}</TableCell>
                <TableCell align="left">{`${price} €`}</TableCell>
                <TableCell align="left">
                  <IconButton
                    aria-label="clearIcon"
                    size="small"
                    sx={{ color: "#1F2C36" }}
                    onClick={async () => {
                      button.onClick(id)
                      await extras.refetch()
                    }}
                  >
                    {button.label}
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
