import db from "db"
import { Ctx } from "blitz"
import * as z from "zod"

const InputValidation = z.object({})

export default async function getUserElectricItems(
  input: z.infer<typeof InputValidation>,
  ctx: Ctx
) {
  ctx.session.$authorize()
  return await db.electricItem.findMany({ where: { userId: ctx.session.userId, deleted: false } })
}
