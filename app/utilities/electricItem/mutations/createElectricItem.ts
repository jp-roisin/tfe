import { resolver, Ctx } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateElectricItemValidation = z.object({
  name: z.string().nonempty(),
  price: z.string().nonempty(),
})

export default async function createElectricItem(
  input: z.infer<typeof CreateElectricItemValidation>,
  ctx: Ctx
) {
  const data = CreateElectricItemValidation.parse(input)

  ctx.session.$authorize()

  return await db.electricItem.create({
    data: { ...data, userId: ctx.session.userId },
  })
}
