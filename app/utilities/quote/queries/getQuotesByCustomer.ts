import db from "db"
import { Ctx } from "blitz"

import * as z from "zod"

const CustomerIdValidation = z.object({
  customerId: z.number(),
})

export default async function getQuotesByCustomer(
  input: z.infer<typeof CustomerIdValidation>,
  ctx: Ctx
) {
  ctx.session.$authorize()
  const data = CustomerIdValidation.parse(input)
  return await db.quote.findMany({ where: { customerId: data.customerId, deleted: false } })
}
