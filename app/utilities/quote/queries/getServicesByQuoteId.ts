import db from "db"
import { Ctx } from "blitz"
import * as z from "zod"

const InputValidation = z.object({
  id: z.number(),
})

export default async function getServicesByQuoteId(
  input: z.infer<typeof InputValidation>,
  ctx: Ctx
) {
  ctx.session.$authorize()
  const data = InputValidation.parse(input)
  return await db.quote.findFirst({
    where: { id: data.id, deleted: false },
    select: { services: true },
  })
}
