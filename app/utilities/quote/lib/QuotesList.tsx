import Table from "@mui/material/Table"
import info from "app/core/assets/info.svg"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import getQuotesByCustomer from "app/utilities/quote/queries/getQuotesByCustomer"
import { Image, Link, useQuery } from "blitz"
import { format } from "date-fns"

export const QuotesList = ({
  customerId,
  complete,
}: {
  customerId: number
  complete?: boolean
}) => {
  const [quotes] = useQuery(getQuotesByCustomer, { customerId })
  return (
    <>
      <Table size="small" aria-label="purchases">
        <TableHead>
          <TableRow>
            <TableCell align="left">Date</TableCell>

            <TableCell align="left">Nom</TableCell>
            {!!complete && <TableCell align="left">Infos</TableCell>}
          </TableRow>
        </TableHead>
        <TableBody>
          {quotes.map(({ id, createdAt }) => (
            <TableRow key={id}>
              <TableCell align="left">{format(createdAt, "dd/MM/yyyy")}</TableCell>
              <TableCell align="left">TODO: Amount</TableCell>
              <TableCell align="left">TODO: PKG</TableCell>
              {!!complete && (
                <TableCell align="left">
                  <Link href={`/quotes/${id}`}>
                    <a>
                      <Image src={info} alt="info" />
                    </a>
                  </Link>
                </TableCell>
              )}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  )
}
