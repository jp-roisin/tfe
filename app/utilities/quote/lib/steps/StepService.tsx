import { CreateArrayOfServices } from "app/utilities/service/lib/CreateArrayOfServices"

export const StepService = ({
  createArrayOfServices,
}: {
  createArrayOfServices: React.ComponentProps<typeof CreateArrayOfServices>
}) => {
  return <CreateArrayOfServices {...createArrayOfServices} />
}
