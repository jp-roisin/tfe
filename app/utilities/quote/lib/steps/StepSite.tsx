import { Button } from "@mui/material"
import { CreateSite } from "app/utilities/site/lib/CreateSite"
import { PickCustomerSite } from "app/utilities/site/lib/PickCustomerSite"
import { useState } from "react"

export const StepSite = ({
  createSite,
  pickCustomerSite,
}: {
  createSite: Omit<React.ComponentProps<typeof CreateSite>, "children">
  pickCustomerSite: Omit<React.ComponentProps<typeof PickCustomerSite>, "children">
}) => {
  const [listSiteMode, setListSiteMode] = useState<boolean>(false)
  const onChangeMode = () => setListSiteMode((prevState) => !prevState)

  return (
    <>
      {!listSiteMode && (
        <CreateSite {...createSite}>
          <Button onClick={onChangeMode} size="small" variant="text" color="primary">
            Le chantier est déjà enregistré ?
          </Button>
        </CreateSite>
      )}
      {listSiteMode && (
        <PickCustomerSite {...pickCustomerSite}>
          <Button onClick={onChangeMode} size="small" variant="text" color="primary">
            Créer un nouveau chantier
          </Button>
        </PickCustomerSite>
      )}
    </>
  )
}
