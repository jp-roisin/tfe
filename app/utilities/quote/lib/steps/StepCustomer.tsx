import { Button } from "@mui/material"
import { CreateCustomer } from "app/utilities/customer/lib/CreateCustomer"
import { CustomersPick } from "app/utilities/customer/lib/CustomersPick"
import { useState } from "react"

export const StepCustomer = ({
  createCustomer,
  customersPick,
}: {
  createCustomer: Omit<React.ComponentProps<typeof CreateCustomer>, "children">
  customersPick: Omit<React.ComponentProps<typeof CustomersPick>, "children">
}) => {
  const [listCustomerMode, setListCustomerMode] = useState<boolean>(false)
  const onChangeMode = () => setListCustomerMode((prevState) => !prevState)

  return (
    <>
      {!listCustomerMode && (
        <CreateCustomer {...createCustomer}>
          <Button onClick={onChangeMode} size="small" variant="text" color="primary">
            Le client est déjà enregistré ?
          </Button>
        </CreateCustomer>
      )}
      {listCustomerMode && (
        <CustomersPick {...customersPick}>
          <Button onClick={onChangeMode} size="small" variant="text" color="primary">
            Créer un nouveau client ?
          </Button>
        </CustomersPick>
      )}
    </>
  )
}
