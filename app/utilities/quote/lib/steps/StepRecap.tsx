import { PrintQuote } from "../PrintQuote"

export const StepRecap = ({
  printQuote,
}: {
  printQuote: React.ComponentProps<typeof PrintQuote>
}) => {
  return (
    <>
      <PrintQuote {...printQuote} />
    </>
  )
}
