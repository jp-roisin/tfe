import { Button } from "@mui/material"
import { MyButton } from "app/core/components/Button"
import { Modal } from "app/core/components/Modal"
import { useState } from "react"

export const PrintQuote = ({
  backButton,
  onSubmit,
  pdf,
}: {
  backButton: Omit<React.ComponentProps<typeof Button>, "variant" | "color">
  onSubmit: () => void
  pdf: React.ReactNode
}) => {
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(false)
  const modalHandler = () => setModalIsOpen((prev) => !prev)
  return (
    <>
      {!!modalIsOpen && <Modal onCloseModal={modalHandler}>{pdf}</Modal>}
      <div className="w-full flex justify-evenly">
        <MyButton variant="outlined" shade="primary" button={{ onClick: backButton.onClick }}>
          {backButton.children}
        </MyButton>
        <MyButton variant="contained" shade="secondary" button={{ onClick: () => modalHandler() }}>
          Inspect
        </MyButton>
        <MyButton variant="contained" shade="primary" button={{ onClick: onSubmit }}>
          Test
        </MyButton>
      </div>
    </>
  )
}
