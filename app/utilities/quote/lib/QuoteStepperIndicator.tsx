import * as React from "react"
import { styled } from "@mui/material/styles"
import Stepper from "@mui/material/Stepper"
import Step from "@mui/material/Step"
import StepLabel from "@mui/material/StepLabel"
import Check from "@mui/icons-material/Check"
import StepConnector, { stepConnectorClasses } from "@mui/material/StepConnector"
import { StepIconProps } from "@mui/material/StepIcon"
import { colors } from "app/core/styles/Constants"
import { StepButton } from "@mui/material"

const QontoConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 10,
    left: "calc(-50% + 16px)",
    right: "calc(50% + 16px)",
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: colors.primary,
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: colors.primary,
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    borderColor: theme.palette.mode === "dark" ? theme.palette.grey[800] : colors.primaryLter,
    borderTopWidth: 3,
    borderRadius: 1,
  },
}))

const QontoStepIconRoot = styled("div")<{ ownerState: { active?: boolean } }>(
  ({ theme, ownerState }) => ({
    color: theme.palette.mode === "dark" ? theme.palette.grey[700] : colors.primaryLter,
    display: "flex",
    height: 22,
    alignItems: "center",
    ...(ownerState.active && {
      color: colors.primary,
    }),
    "& .QontoStepIcon-completedIcon": {
      color: colors.primary,
      zIndex: 1,
      fontSize: 20,
    },
    "& .QontoStepIcon-circle": {
      width: 8,
      height: 8,
      borderRadius: "50%",
      backgroundColor: colors.primaryLt,
    },
  })
)

const QontoStepIcon = (props: StepIconProps) => {
  const { active, completed, className } = props

  return (
    <QontoStepIconRoot ownerState={{ active }} className={className}>
      {completed ? (
        <Check className="QontoStepIcon-completedIcon" />
      ) : (
        <div className="QontoStepIcon-circle" />
      )}
    </QontoStepIconRoot>
  )
}

export const QuoteStepperIndicator = ({
  currentStep,
  onStepClick,
  steps,
}: {
  currentStep: number
  onStepClick: (stepIndex: number) => void
  steps: {
    label: string
    enabled: boolean
    completed: boolean
  }[]
}) => {
  return (
    <Stepper nonLinear alternativeLabel activeStep={currentStep} connector={<QontoConnector />}>
      {steps.map(({ label, enabled, completed }, i) => (
        <Step key={label}>
          <StepButton onClick={() => enabled && onStepClick(i)}>
            <StepLabel StepIconProps={{ completed }} StepIconComponent={QontoStepIcon}>
              {label}
            </StepLabel>
          </StepButton>
        </Step>
      ))}
    </Stepper>
  )
}
