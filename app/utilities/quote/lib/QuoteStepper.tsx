import { Suspense, useState } from "react"
import { QuoteStepperIndicator } from "./QuoteStepperIndicator"
import { Service, Customer, Site } from "db"
import { StepService } from "./steps/StepService"
import { StepCustomer } from "./steps/StepCustomer"
import { StepSite } from "./steps/StepSite"
import type { SetOptional } from "type-fest"
import createQuote from "../mutations/createQuote"
import { useMutation } from "blitz"
import { notUndefined } from "app/lib/utils"
import { StepRecap } from "./steps/StepRecap"

export type QuoteForm = {
  currentStep: number
  customer?: SetOptional<Customer, "id" | "createdAt" | "updatedAt" | "userId" | "deleted">
  site?: SetOptional<Site, "id" | "createdAt" | "updatedAt" | "customerId" | "deleted">
  services: Service[]
}

export const QuoteStepper = () => {
  const [stepperState, setStepperState] = useState<QuoteForm>({ currentStep: 0, services: [] })
  const [createQuoteMutation] = useMutation(createQuote)
  const onQuoteStepperStepClick = (value: number) => {
    setStepperState({ ...stepperState, currentStep: value })
  }
  return (
    <>
      <div className="my-12">
        <QuoteStepperIndicator
          currentStep={stepperState.currentStep}
          onStepClick={onQuoteStepperStepClick}
          steps={[
            { label: "Client", enabled: true, completed: !!stepperState.customer },
            { label: "Chantier", enabled: !!stepperState.customer, completed: !!stepperState.site },
            { label: "Services", enabled: true, completed: stepperState.services.length !== 0 },
            { label: "Recap", enabled: !!stepperState.site, completed: false },
          ]}
        />
      </div>
      <Suspense fallback="Loading ...">
        {stepperState.currentStep === 0 && (
          <>
            <StepCustomer
              createCustomer={{
                submitText: "Next",
                initialValues: {
                  ...stepperState.customer,
                },
                backButton: {
                  onClick: () => setStepperState({ ...stepperState, currentStep: 0 }),
                  children: "Back",
                },
                onSubmit: (values) =>
                  setStepperState({
                    ...stepperState,
                    currentStep: 1,
                    customer: { ...values },
                  }),
              }}
              customersPick={{
                backButton: {
                  onClick: () => setStepperState({ ...stepperState, currentStep: 0 }),
                  label: "Back",
                },
                nextButton: {
                  onNextStep: (values) =>
                    setStepperState({
                      ...stepperState,
                      currentStep: 1,
                      customer: { ...values },
                    }),
                  label: "Next",
                },
              }}
            />
          </>
        )}
        {stepperState.currentStep === 1 && (
          <>
            <StepSite
              createSite={{
                submitText: "Next",
                backButton: {
                  children: "Back",
                  onClick: () => setStepperState({ ...stepperState, currentStep: 0 }),
                },
                initialValues: { ...stepperState.site },
                onSubmit: (values) =>
                  setStepperState({
                    ...stepperState,
                    currentStep: 2,
                    site: { ...values },
                  }),
              }}
              pickCustomerSite={{
                customer: notUndefined(stepperState.customer),
                backButton: {
                  label: "Back",
                  onClick: () => setStepperState({ ...stepperState, currentStep: 0 }),
                },
                nextButton: {
                  label: "Next",
                  onNextStep: (values) => {
                    setStepperState({
                      ...stepperState,
                      currentStep: 2,
                      site: { ...values },
                    })
                  },
                },
              }}
            />
          </>
        )}
        {stepperState.currentStep === 2 && (
          <>
            <StepService
              createArrayOfServices={{
                submitText: "Next",
                prevValues: stepperState.services || [],
                onSubmit: (values) =>
                  setStepperState({
                    ...stepperState,
                    currentStep: 3,
                    services: [...values],
                  }),
              }}
            />
          </>
        )}
        {stepperState.currentStep === 3 && (
          <StepRecap
            printQuote={{
              backButton: {
                children: "back",
                onClick: () => setStepperState({ ...stepperState, currentStep: 3 }),
              },
              onSubmit: () => {
                const { customer, site, services, currentStep, ...data } = stepperState
                if (!customer) throw new Error("customer not found")
                if (!site) throw new Error("site not found")
                createQuoteMutation({
                  ...data,
                  customer,
                  site,
                  serviceIds: services.map((s) => s.id),
                })
              },
              // TODO: add pdf generation
              // pdf: <MyDocument info={stepperState} />,
              pdf: <></>,
            }}
          />
        )}
      </Suspense>
    </>
  )
}
