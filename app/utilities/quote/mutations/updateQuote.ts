import { resolver } from "blitz"
import db from "db"
import * as z from "zod"

export const UpdateServiceValidation = z.object({
  id: z.number(),
  serviceIds: z.array(z.number()),
})

export default resolver.pipe(resolver.zod(UpdateServiceValidation), resolver.authorize(), (data) =>
  db.quote.update({
    where: { id: data.id },
    data: {
      services: {
        connect: data.serviceIds.map((id) => ({ id })),
      },
    },
  })
)
