import { resolver, NotFoundError } from "blitz"
import db from "db"
import * as z from "zod"
import updateCustomer from "app/utilities/customer/mutations/updateCustomer"
import updateSite from "app/utilities/site/mutations/updateSite"
import { CreateSiteValidation } from "app/utilities/site/mutations/createSite"
import { CreateCustomerValidation } from "app/utilities/customer/mutations/createCustomer"
import createSite from "app/utilities/site/mutations/createSite"
import createCustomer from "app/utilities/customer/mutations/createCustomer"

export const CreateQuoteValidation = z.object({
  customer: CreateCustomerValidation.extend({
    id: z.number().optional(),
  }),
  site: CreateSiteValidation.extend({
    id: z.number().optional(),
    customerId: z.number().optional().nullable(),
  }),
  serviceIds: z.array(z.number()),
})

export default resolver.pipe(
  resolver.zod(CreateQuoteValidation),
  resolver.authorize(),
  async (data, ctx) => {
    const { customer: customerData, site: siteData, serviceIds, ...quoteData } = data

    const customer = customerData.id
      ? await updateCustomer({ ...customerData, id: customerData.id }, ctx)
      : await createCustomer(customerData, ctx)

    if (!customer) {
      throw new NotFoundError()
    }

    const site = siteData.id
      ? await updateSite(
          {
            ...siteData,
            id: siteData.id,
            customerId: customer.id,
          },
          ctx
        )
      : await createSite(
          {
            ...siteData,
            customerId: customer.id,
          },
          ctx
        )

    const quote = await db.quote.create({
      data: {
        customerId: customer.id,
        siteId: site.id,
        ...quoteData,

        services: {
          connect: serviceIds.map((id) => ({ id })),
        },
        userId: ctx.session.userId,
      },
    })

    return quote
  }
)
