import { resolver } from "blitz"
import db from "db"
import { z } from "zod"

export default resolver.pipe(resolver.zod(z.number()), resolver.authorize(), async (id) => {
  return await db.quote.update({ where: { id }, data: { deleted: true } })
})
