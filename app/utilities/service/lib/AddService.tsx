import { MyButton } from "app/core/components/Button"
import Form from "app/core/components/Form"
import { useMutation } from "blitz"
import { TextField } from "mui-rff"
import createService, { CreateServiceValidation } from "../mutations/createService"

export const AddService = ({ refreshList }: { refreshList: () => void }) => {
  const [addService] = useMutation(createService)

  return (
    <>
      <Form
        onSubmit={async (values) => {
          try {
            await addService(values)
            refreshList()
          } catch (error) {
            console.log(error)
          }
        }}
        schema={CreateServiceValidation}
      >
        <div className="flex">
          <div className="flex-1 mr-4">
            <TextField size="small" name="name" label="Nouveau service" fullWidth />
          </div>
          <div className="flex-3 mr-4">
            <TextField size="small" name="price" label="Tarif" fullWidth />
          </div>
          <MyButton
            variant="contained"
            shade="secondary"
            button={{ size: "small", type: "submit" }}
          >
            Ajouter
          </MyButton>
        </div>
      </Form>
    </>
  )
}
