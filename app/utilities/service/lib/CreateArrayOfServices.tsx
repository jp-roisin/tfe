import { ListServices } from "./ListServices"
import { useEffect, useState } from "react"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { IconButton } from "@mui/material"
import { Subtitle } from "app/core/components/Subtitile"
import { Service } from "db"
import { MyButton } from "app/core/components/Button"
import classNames from "classnames"

export const CreateArrayOfServices = ({
  submitText,
  onSubmit,
  prevValues,
  deleteButton,
}: {
  submitText: string
  onSubmit: (values: Service[]) => void
  prevValues: Service[]
  deleteButton?: {
    label: React.ReactNode | string
    onClick: () => void
  }
}) => {
  const [serviceList, setServiceList] = useState<Service[]>([...prevValues])

  const addServiceHandler = (item: Service) => {
    setServiceList((prevServiceList) => [...prevServiceList, item])
  }

  const removeServiceHandler = (item: Service) => {
    const index = serviceList.findIndex((element) => element === item)
    setServiceList((prevServiceList) => {
      prevServiceList.splice(index, 1)
      return [...prevServiceList]
    })
  }

  const validateArrayHandler = () => {
    onSubmit(serviceList)
  }

  return (
    <>
      <ListServices button={{ label: "+", onClick: addServiceHandler }} />
      <Subtitle>Ajouter au devis</Subtitle>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 600 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell align="left">Service</TableCell>
              <TableCell align="left">Prix</TableCell>
              <TableCell align="left"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {serviceList.map((item, i) => (
              <TableRow key={item.id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell align="left">{item.name}</TableCell>
                <TableCell align="left">{`${item.price} €`}</TableCell>
                <TableCell align="left">
                  <IconButton
                    aria-label="clearIcon"
                    size="small"
                    sx={{ color: "#1F2C36" }}
                    onClick={() => {
                      removeServiceHandler(item)
                    }}
                  >
                    -
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div
        className={classNames("w-full flex justify-end my-8", !!deleteButton && "justify-between")}
      >
        {!!deleteButton && (
          <MyButton
            variant="contained"
            shade="ternary"
            button={{ onClick: () => deleteButton.onClick() }}
          >
            {deleteButton.label}
          </MyButton>
        )}
        <MyButton variant="contained" shade="primary" button={{ onClick: validateArrayHandler }}>
          {submitText}
        </MyButton>
      </div>
    </>
  )
}
