import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { useQuery } from "blitz"
import { IconButton } from "@mui/material"
import getUserServices from "app/utilities/service/queries/getUserServices"
import { AddService } from "app/utilities/service/lib/AddService"
import { Subtitle } from "app/core/components/Subtitile"
import { Service } from "db"

export const ListServices = ({
  button,
}: {
  button: {
    onClick: (item: Service) => void
    label: string | React.ReactNode
  }
}) => {
  const [userServices, extras] = useQuery(getUserServices, {})

  const refreshListHandler = () => {
    extras.refetch()
  }
  return (
    <>
      <Subtitle>Liste de vos services</Subtitle>
      <div className="my-8">
        <AddService refreshList={refreshListHandler} />
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 600 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell align="left">Service</TableCell>
              <TableCell align="left">Prix</TableCell>
              <TableCell align="left"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userServices.map((item, i) => (
              <TableRow key={item.id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell align="left">{item.name}</TableCell>
                <TableCell align="left">{`${item.price} €`}</TableCell>
                <TableCell align="left">
                  <IconButton
                    aria-label="clearIcon"
                    size="small"
                    sx={{ color: "#1F2C36" }}
                    onClick={async () => {
                      button.onClick(item)
                      await extras.refetch()
                    }}
                  >
                    {button.label}
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
