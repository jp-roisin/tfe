import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateServiceValidation = z.object({
  name: z.string().nonempty(),
  price: z.string().nonempty(),
})

export default async function createService(
  input: z.infer<typeof CreateServiceValidation>,
  ctx: Ctx
) {
  const data = CreateServiceValidation.parse(input)

  ctx.session.$authorize()

  const service = await db.service.create({
    data: { ...data, userId: ctx.session.userId },
  })

  return service
}
