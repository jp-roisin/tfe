import { v4 as uuidv4 } from "uuid"
import { useMutation } from "blitz"
import createActivationKey from "../mutations/createActiavationKey"
import AddIcon from "@mui/icons-material/Add"
import { MyButton } from "app/core/components/Button"

const GenerateActivationKey = ({ refreshList }: { refreshList: () => void }) => {
  const [generateKey] = useMutation(createActivationKey)

  const generateKeyHandler = async () => {
    try {
      await generateKey({ key: uuidv4() }, undefined)
      refreshList()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <MyButton
      variant="contained"
      shade="secondary"
      button={{ startIcon: <AddIcon />, onClick: generateKeyHandler }}
    >
      Nouvelle clé
    </MyButton>
  )
}

export default GenerateActivationKey
