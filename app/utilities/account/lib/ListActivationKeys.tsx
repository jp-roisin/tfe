import { useQuery, useMutation } from "blitz"
import getActivationKeys from "app/utilities/account/queries/getActivationKeys"
import { Suspense } from "react"
import deleteActivationKey from "../mutations/deleteActivationKey"
import { format } from "date-fns"
import IconButton from "@mui/material/IconButton"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import GenerateActivationKey from "./GenerateActivationKey"

const ActivationList = () => {
  const [activationKeys, extras] = useQuery(getActivationKeys, {})
  const [deleteActivationKeyMutation] = useMutation(deleteActivationKey)

  const refreshListHandler = () => {
    extras.refetch()
  }

  return (
    <>
      <div className="my-8">
        <GenerateActivationKey refreshList={refreshListHandler} />
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 600 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell align="left">Clé</TableCell>
              <TableCell align="left">Utilisateur</TableCell>
              <TableCell align="left">Modifié le</TableCell>
              <TableCell align="left">Supprimer</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {activationKeys.map((item, i) => (
              <TableRow key={item.id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell align="left">{item.key}</TableCell>
                <TableCell align="left">
                  {item.userId === null ? "..." : item.user?.email}
                </TableCell>
                <TableCell align="left">{format(item.updatedAt, "dd/MM/yyyy")}</TableCell>
                <TableCell align="left">
                  <IconButton
                    aria-label="clearIcon"
                    size="small"
                    color="error"
                    onClick={async () => {
                      await deleteActivationKeyMutation(item.id)
                      await extras.refetch()
                    }}
                  >
                    X
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

export const ActivationKeys = () => {
  return (
    <div>
      <Suspense fallback="Loading ...">
        <ActivationList />
      </Suspense>
    </div>
  )
}
