import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

const CreateKey = z.object({
  key: z.string().nonempty(),
})

export default async function createActivationKey(input: z.infer<typeof CreateKey>, ctx: Ctx) {
  const data = CreateKey.parse(input)
  ctx.session.$authorize()

  return await db.activation.create({ data })
}
