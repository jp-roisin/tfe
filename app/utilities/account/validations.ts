import { z } from "zod"

export const CreateServiceValidation = z.object({
  service: z.string().optional(),
  price: z.string().optional(),
})
