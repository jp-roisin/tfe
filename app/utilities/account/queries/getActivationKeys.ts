import { Ctx } from "blitz"
import db from "db"
import * as z from "zod"

const KeyValidation = z.object({})

export default async function getActivationKeys(input: z.infer<typeof KeyValidation>, ctx: Ctx) {
  ctx.session.$authorize()

  return await db.activation.findMany({
    include: {
      user: true,
    },
    where: {
      deleted: false,
    },
  })
}
