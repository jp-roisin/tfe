import { useQuery, useSession, useMutation } from "blitz"
import getUserData from "../queries/getUserData"
import { TextField } from "mui-rff"

import { InputValidation as UpdateUserSchema } from "../mutations/updateUserData"
import { MyButton } from "app/core/components/Button"
import updateUserData from "../mutations/updateUserData"
import Form from "app/core/components/Form"
import { Subtitle } from "app/core/components/Subtitile"
import AccountIcon from "app/core/assets/basil/AccountIcon-46"
import FireIcon from "app/core/assets/basil/FireIcon-46"
import { colors } from "app/core/styles/Constants"

export const UserUpdateForm = () => {
  const { userId } = useSession()
  const [userData] = useQuery(getUserData, {})
  const [updateUserMutation] = useMutation(updateUserData)

  return (
    <>
      <Subtitle>Informations personnelles</Subtitle>
      <Form
        className="m-6"
        schema={UpdateUserSchema}
        initialValues={userData}
        onSubmit={async (values) => {
          try {
            await updateUserMutation(values)
          } catch (error) {
            console.log(error)
          }
        }}
      >
        <div className="mb-6 flex flex-row max-w-4xl">
          <div className="flex-none w-20 border-r-4 mr-4 border-primaryLt justify-center items-center">
            <div className="flex flex-col items-center mr-8">
              <AccountIcon fill={colors.primary} />
              <h3 className="text-xl italic">Vous</h3>
            </div>
          </div>
          <div className="flex-1 ">
            <div className="flex">
              <div className="flex-1 mr-8">
                <TextField
                  required
                  name="firstName"
                  label="Prénom"
                  variant="standard"
                  margin="normal"
                  fullWidth
                />
              </div>
              <div className="flex-1">
                <TextField
                  required
                  name="lastName"
                  label="Nom"
                  variant="standard"
                  margin="normal"
                  fullWidth
                />
              </div>
            </div>
            <div className="flex flex-col">
              <TextField
                required
                name="email"
                label="Email"
                variant="standard"
                fullWidth
                margin="normal"
              />
              <TextField
                required
                name="phone"
                label="Téléphone"
                variant="standard"
                margin="normal"
              />
            </div>
          </div>
        </div>
        <div className="mb-12 flex flex-row max-w-4xl">
          <div className="flex-none w-20 border-r-4 mr-4 border-primaryLt justify-center items-center">
            <div className="flex flex-col items-center mr-8">
              <FireIcon fill={colors.primary} />
              <h3 className="text-xl italic">Société</h3>
            </div>
          </div>
          <div className="flex-1 ">
            <div className="flex">
              <div className="flex-1 mr-8">
                <TextField
                  required
                  name="companyName"
                  label="Nom de la société"
                  variant="standard"
                  margin="normal"
                  fullWidth
                />
              </div>
              <div className="flex-1">
                <TextField
                  required
                  name="tva"
                  label="TVA"
                  variant="standard"
                  margin="normal"
                  fullWidth
                />
              </div>
            </div>
            <TextField
              required
              name="adressCountry"
              label="Pays"
              variant="standard"
              margin="normal"
              fullWidth
            />
            <div className="flex">
              <div className="flex-1 mr-8">
                <TextField
                  required
                  name="adressLocality"
                  label="Localité"
                  variant="standard"
                  margin="normal"
                  fullWidth
                />
              </div>
              <div className="flex-1">
                <TextField
                  required
                  name="adressPostal"
                  label="Code postal"
                  variant="standard"
                  margin="normal"
                  fullWidth
                />
              </div>
            </div>
            <div className="flex flex-col">
              <TextField
                required
                name="adressStreet"
                label="Adresse"
                variant="standard"
                margin="normal"
              />
            </div>
          </div>
        </div>
        <div className="w-full flex justify-end">
          <MyButton shade="secondary" variant="contained" button={{ type: "submit" }}>
            Enregistrer
          </MyButton>
        </div>
      </Form>
    </>
  )
}
