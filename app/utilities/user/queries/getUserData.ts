import db from "db"
import { Ctx } from "blitz"
import * as z from "zod"

const UserValidation = z.object({})

export default async function getUserData(input: z.infer<typeof UserValidation>, ctx: Ctx) {
  ctx.session.$authorize()
  return await db.user.findFirst({ where: { id: ctx.session.userId } })
}
