import { Ctx, NotFoundError, resolver } from "blitz"
import db from "db"
import * as z from "zod"

const email = z
  .string()
  .email()
  .transform((str) => str.toLowerCase().trim())

export const InputValidation = z.object({
  firstName: z.string().optional(),
  lastName: z.string().optional(),
  email: email,
  phone: z.string().optional(),
  companyName: z.string().optional(),
  tva: z.string().optional(),
  adressCountry: z.string().optional(),
  adressLocality: z.string().optional(),
  adressPostal: z.string().optional(),
  adressStreet: z.string().optional(),
})

export default resolver.pipe(
  resolver.zod(InputValidation),
  resolver.authorize(),
  async (
    {
      firstName,
      lastName,
      email,
      phone,
      companyName,
      tva,
      adressCountry,
      adressLocality,
      adressPostal,
      adressStreet,
    },
    ctx: Ctx
  ) => {
    const user = await db.user.findFirst({ where: { id: ctx.session.userId! } })
    if (ctx.session.userId) {
      await db.user.update({
        where: { id: ctx.session.userId },
        data: {
          firstName,
          lastName,
          email,
          phone,
          companyName,
          tva,
          adressCountry,
          adressLocality,
          adressPostal,
          adressStreet,
        },
      })
    } else {
      throw new NotFoundError()
    }

    return user
  }
)
