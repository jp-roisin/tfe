import { MyButton } from "app/core/components/Button"
import logout from "app/utilities/auth/mutations/logout"
import { useMutation } from "blitz"

export const LogoutButton = () => {
  const [logoutMutation] = useMutation(logout)

  return (
    <MyButton variant="contained" shade="ternary" button={{ onClick: () => logoutMutation }}>
      Logout
    </MyButton>
  )
}
