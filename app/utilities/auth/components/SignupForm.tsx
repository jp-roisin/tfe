import { useMutation } from "blitz"
import { LabeledTextField } from "app/core/components/LabeledTextField"
import { Form, FORM_ERROR } from "app/core/components/Form"
import signup from "app/utilities/auth/mutations/signup"
import { Signup } from "app/utilities/auth/validations"
import { Button } from "@mui/material"

type SignupFormProps = {
  onSuccess?: () => void
}

export const SignupForm = ({ onSuccess }: SignupFormProps) => {
  const [signupMutation] = useMutation(signup)

  return (
    <div>
      <h1 className="text-white">Create an Account</h1>

      <Form
        submitText="Create Account"
        schema={Signup}
        initialValues={{ email: "", password: "", activationKey: "" }}
        onSubmit={async (values) => {
          try {
            await signupMutation(values)
            onSuccess?.()
          } catch (error: any) {
            if (error.code === "P2002" && error.meta?.target?.includes("email")) {
              // This error comes from Prisma
              return { email: "This email is already being used" }
              // }
              // if (error.meta?.target?.includes("activationKey")) {
              //   return { activationKey: "This is not a valid activation key" }
            } else {
              return { [FORM_ERROR]: error.toString() }
            }
          }
        }}
      >
        <LabeledTextField name="email" label="Email" placeholder="Email" />
        <LabeledTextField name="password" label="Password" placeholder="Password" type="password" />
        <LabeledTextField
          name="activationKey"
          label="ActivationKey"
          placeholder="Activation Key"
          type="text"
        />
        <Button type="submit" variant="contained">
          Signup
        </Button>
      </Form>
    </div>
  )
}

export default SignupForm
