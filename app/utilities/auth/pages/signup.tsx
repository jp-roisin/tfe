import { useRouter, BlitzPage, Routes, Link } from "blitz"
import { LayoutBase } from "app/core/layouts/LayoutBase"
import { SignupForm } from "app/utilities/auth/components/SignupForm"
import { Card } from "app/core/layouts/Card"

const SignupPage: BlitzPage = () => {
  const router = useRouter()

  return (
    <Card classes="text-center absolute top-1/2 right-1/2 translate-x-1/2 translate-y-[-50%] bg-sidebar">
      <SignupForm onSuccess={() => router.push(Routes.Home())} />

      <Link href={Routes.LoginPage()}>
        <a className="button small text-white">
          <strong>Already an account ?</strong>
        </a>
      </Link>
    </Card>
  )
}

SignupPage.redirectAuthenticatedTo = "/"
SignupPage.getLayout = (page) => <LayoutBase title="Sign Up">{page}</LayoutBase>

export default SignupPage
