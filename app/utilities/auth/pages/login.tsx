import { useRouter, BlitzPage } from "blitz"
import { LoginForm } from "app/utilities/auth/components/LoginForm"
import { LayoutBase } from "app/core/layouts/LayoutBase"
import { Card } from "app/core/layouts/Card"

const LoginPage: BlitzPage = () => {
  const router = useRouter()

  return (
    <Card classes="text-center absolute top-1/2 right-1/2 translate-x-1/2 translate-y-[-50%] bg-sidebar ">
      <LoginForm
        onSuccess={(_user) => {
          router.push("/account")
        }}
      />
    </Card>
  )
}

LoginPage.redirectAuthenticatedTo = "/"
LoginPage.getLayout = (page) => <LayoutBase title="Log In">{page}</LayoutBase>

export default LoginPage
