import { AuthenticationError, resolver, SecurePassword } from "blitz"
import db from "db"
import { Signup } from "app/utilities/auth/validations"
import { Role } from "types"

export default resolver.pipe(resolver.zod(Signup), async (obj, ctx) => {
  const { email, password, activationKey } = obj
  const activedKey = await db.activation.findFirst({
    where: {
      key: {
        equals: activationKey.toLowerCase().trim(),
      },
    },
  })
  if (activedKey !== null) {
    const hashedPassword = await SecurePassword.hash(password.trim())
    const user = await db.user.create({
      data: { email: email.toLowerCase().trim(), hashedPassword, role: "USER", activationKey },
      select: { id: true, email: true, role: true, activationKey: true },
    })
    await ctx.session.$create({ userId: user.id, role: user.role as Role })
    await db.activation.update({
      where: { key: activationKey },
      data: { userId: user.id },
    })
    return user
  } else {
    throw new AuthenticationError()
  }
})
