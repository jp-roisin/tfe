import { resolver } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateSiteValidation = z.object({
  adressCountry: z.string().nonempty(),
  adressLocality: z.string().nonempty(),
  adressPostal: z.string().nonempty(),
  adressStreet: z.string().nonempty(),
  customerId: z.number(),
})

export default resolver.pipe(resolver.zod(CreateSiteValidation), resolver.authorize(), (data) =>
  db.site.create({
    data,
  })
)
