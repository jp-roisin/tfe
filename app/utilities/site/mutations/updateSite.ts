import { resolver } from "blitz"
import db from "db"
import * as z from "zod"
import { CreateSiteValidation } from "./createSite"

export const UpdateSiteValidation = CreateSiteValidation.extend({
  id: z.number(),
})

export default resolver.pipe(resolver.zod(UpdateSiteValidation), resolver.authorize(), (data) =>
  db.site.update({
    where: { id: data.id },
    data,
  })
)
