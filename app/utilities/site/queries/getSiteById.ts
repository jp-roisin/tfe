import db from "db"
import { Ctx } from "blitz"
import * as z from "zod"

const InputValidation = z.object({
  id: z.number(),
})

export default async function getSiteById(input: z.infer<typeof InputValidation>, ctx: Ctx) {
  ctx.session.$authorize()
  const data = InputValidation.parse(input)
  return await db.site.findFirst({ where: { id: data.id, deleted: false } })
}
