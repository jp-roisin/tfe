import db from "db"
import { Ctx } from "blitz"
import * as z from "zod"

const InputValidation = z.object({
  customerId: z.number(),
})

export default async function getSitesByCustomerId(
  input: z.infer<typeof InputValidation>,
  ctx: Ctx
) {
  ctx.session.$authorize()
  const data = InputValidation.parse(input)
  return await db.site.findMany({ where: { customerId: data.customerId, deleted: false } })
}
