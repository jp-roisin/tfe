import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { useQuery } from "blitz"
import { Subtitle } from "app/core/components/Subtitile"
import { Site, Customer } from "db"
import { useState } from "react"
import { colors } from "app/core/styles/Constants"
import { SetOptional } from "type-fest"
import { MyButton } from "app/core/components/Button"
import getSitesByCustomerId from "../queries/getSitesByCustomerId"

export const PickCustomerSite = ({
  customer,
  backButton,
  nextButton,
  children,
}: {
  customer: SetOptional<Pick<Customer, "id" | "firstName" | "lastName">, "id">
  backButton?: {
    label: string
    onClick: () => void
  }
  nextButton?: {
    label: string
    onNextStep: (arg: Site) => void
  }
  children: React.ReactNode
}) => {
  const [sitePicked, setSitePicked] = useState<Site>()
  const [customerSites] = useQuery(
    getSitesByCustomerId,
    { customerId: customer.id as number },
    { enabled: !!customer.id }
  )
  const nextButtonHandler = () => {
    if (sitePicked && !!nextButton) {
      nextButton.onNextStep(sitePicked)
    }
  }
  const subtitle = `Chantiers de ${customer?.firstName} ${customer?.lastName}`
  return (
    <>
      {customerSites && customerSites.length === 0 && (
        <>
          <div className="flex w-full justify-between">
            <Subtitle>{subtitle}</Subtitle>
            {children}
          </div>
          <p>{"Ce client n'a aucun chantier enregistré pour l'instant."}</p>
        </>
      )}
      {customerSites && customerSites.length !== 0 && (
        <>
          <div className="flex w-full justify-between">
            <Subtitle>{subtitle}</Subtitle>
            {children}
          </div>
          {customerSites !== [] && customerSites !== undefined && (
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 600 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Code Postal</TableCell>
                    <TableCell align="left">Localité</TableCell>
                    <TableCell align="left">Rue</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {customerSites.map((item) => (
                    <TableRow
                      onClick={() => {
                        setSitePicked(item)
                      }}
                      key={item.id}
                      sx={
                        !!sitePicked && sitePicked.id === item.id
                          ? { bgcolor: colors.primaryLter, cursor: "pointer" }
                          : { cursor: "pointer" }
                      }
                    >
                      <TableCell align="left">{item.adressPostal}</TableCell>
                      <TableCell align="left">{item.adressLocality}</TableCell>
                      <TableCell align="left">{item.adressStreet}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </>
      )}
      {!customerSites && (
        <>
          <div className="flex w-full justify-between">
            <Subtitle>Attention !</Subtitle>
            {children}
          </div>
          <p>
            {"Vous ne pouvez pas lister les chantiers d'un client qui n'est pas encore enregistré."}
          </p>
        </>
      )}
      {!!backButton && !!nextButton && (
        <div className="mt-6 flex w-full justify-end">
          <div className="mr-4">
            <MyButton variant="outlined" shade="primary" button={{ onClick: backButton.onClick }}>
              {backButton.label}
            </MyButton>
          </div>
          <MyButton variant="contained" shade="primary" button={{ onClick: nextButtonHandler }}>
            {nextButton.label}
          </MyButton>
        </div>
      )}
    </>
  )
}
