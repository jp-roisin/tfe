import Form from "app/core/components/Form"
import { TextField } from "mui-rff"
import { CreateSiteValidation } from "app/utilities/site/mutations/createSite"
import { Link, useMutation, useQuery, useRouter } from "blitz"
import getSiteById from "../queries/getSiteById"
import { MyButton } from "app/core/components/Button"
import updateSite from "../mutations/updateSite"
import deleteSite from "../mutations/deleteSite"
import { Subtitle } from "app/core/components/Subtitile"
import { Button } from "@mui/material"

export const SiteUpdate = ({}: {}) => {
  const { query } = useRouter()
  const router = useRouter()
  const site = useQuery(getSiteById, { id: Number(query.id) })
  const [updateSiteMutation] = useMutation(updateSite)
  const [deleteSiteMutation] = useMutation(deleteSite)
  const redirect = site[0]?.customerId ? `/customers/${site[0].customerId}/sites` : "#"
  return (
    <>
      <div className="my-6 flex justify-between items-center">
        <Subtitle>Mise à jour</Subtitle>
        <div>
          <Link href={redirect}>
            <a>
              <Button variant="text">Retour</Button>
            </a>
          </Link>
        </div>
      </div>
      <Form
        initialValues={{ ...site[0] }}
        schema={CreateSiteValidation}
        onSubmit={async (values) => {
          try {
            await updateSiteMutation({ ...values, id: Number(query.id) })
            await router.push(redirect)
          } catch (error) {
            console.log(error)
          }
        }}
      >
        <TextField name="adressCountry" label="Pays" variant="standard" margin="normal" fullWidth />
        <TextField
          size="small"
          name="adressLocality"
          label="Localité"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressPostal"
          label="Code postal"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressStreet"
          label="Rue"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <div className="w-full flex justify-between">
          <MyButton
            variant="contained"
            shade="ternary"
            button={{
              onClick: async () => {
                await deleteSiteMutation(Number(query.id))
                await router.push(redirect)
              },
            }}
          >
            Supprimer
          </MyButton>
          <MyButton variant="contained" shade="secondary" button={{ type: "submit" }}>
            Enregistrer
          </MyButton>
        </div>
      </Form>
    </>
  )
}
