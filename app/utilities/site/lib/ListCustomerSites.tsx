import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { Image, Link, useQuery } from "blitz"
import getSitesByCustomerId from "../queries/getSitesByCustomerId"
import info from "app/core/assets/info.svg"

export const ListCustomerSites = ({ customerId }: { customerId: number }) => {
  const [customerSites] = useQuery(getSitesByCustomerId, { customerId })

  return (
    <>
      {customerSites && customerSites.length === 0 && (
        <p>{"Ce client n'a aucun chantier enregistré pour l'instant."}</p>
      )}
      {customerSites && customerSites.length !== 0 && (
        <>
          {customerSites !== [] && customerSites !== undefined && (
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 600 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Code Postal</TableCell>
                    <TableCell align="left">Localité</TableCell>
                    <TableCell align="left">Rue</TableCell>
                    <TableCell align="left">Infos</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {customerSites.map((item, i) => (
                    <TableRow onClick={() => console.log} key={i}>
                      <TableCell align="left">{item.adressPostal}</TableCell>
                      <TableCell align="left">{item.adressLocality}</TableCell>
                      <TableCell align="left">{item.adressStreet}</TableCell>
                      <TableCell align="left">
                        <Link href={`/sites/${item.id}`}>
                          <a>
                            <Image src={info} alt="info" />
                          </a>
                        </Link>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </>
      )}
      {!customerSites && (
        <>
          <p>
            {"Vous ne pouvez pas lister les chantiers d'un client qui n'est pas encore enregistré."}
          </p>
        </>
      )}
    </>
  )
}
