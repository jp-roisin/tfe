import Form, { FormProps } from "app/core/components/Form"
import { TextField } from "mui-rff"
import { CreateSiteValidation } from "app/utilities/site/mutations/createSite"
import { Button } from "@mui/material"
import { Subtitle } from "app/core/components/Subtitile"
import { MyButton } from "app/core/components/Button"

export const CreateSite = ({
  submitText,
  onSubmit,
  initialValues,
  backButton,
  children,
}: {
  submitText?: string
  onSubmit: FormProps<typeof CreateSiteValidation>["onSubmit"]
  initialValues?: FormProps<typeof CreateSiteValidation>["initialValues"]
  backButton?: Omit<React.ComponentProps<typeof Button>, "variant" | "color">
  children: React.ReactNode
}) => {
  return (
    <>
      <div className="flex w-full items-center justify-between">
        <Subtitle>Nouveau chantier</Subtitle>
        {children}
      </div>
      <Form initialValues={initialValues} schema={CreateSiteValidation} onSubmit={onSubmit}>
        <TextField name="adressCountry" label="Pays" variant="standard" margin="normal" fullWidth />
        <TextField
          size="small"
          name="adressLocality"
          label="Localité"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressPostal"
          label="Code postal"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressStreet"
          label="Rue"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <div className="w-full flex justify-end">
          {!!backButton && (
            <div className="mr-4">
              <MyButton variant="outlined" shade="primary" button={{ onClick: backButton.onClick }}>
                {backButton.children}
              </MyButton>
            </div>
          )}
          <MyButton variant="contained" shade="primary" button={{ type: "submit" }}>
            {submitText}
          </MyButton>
        </div>
      </Form>
    </>
  )
}
