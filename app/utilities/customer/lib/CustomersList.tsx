import { Suspense, useState } from "react"
import Collapse from "@mui/material/Collapse"
import IconButton from "@mui/material/IconButton"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp"
import { Image, Link, useQuery } from "blitz"
import getUserCustomers from "app/utilities/customer/queries/getUserCustomers"
import { Customer } from "db"
import info from "app/core/assets/info.svg"
import { QuotesList } from "app/utilities/quote/lib/QuotesList"
import { Box, Typography } from "@mui/material"

const Item = ({ item }: { item: Customer }) => {
  const { id, firstName, lastName, email, phone } = item
  const [open, setOpen] = useState(false)

  return (
    <>
      <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align="left">{lastName}</TableCell>
        <TableCell align="left">{firstName}</TableCell>
        <TableCell align="left">{email}</TableCell>
        <TableCell align="left">{phone}</TableCell>
        <TableCell align="left">
          <Link href={`/customers/${id}`}>
            <a>
              <Image src={info} alt="info" />
            </a>
          </Link>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Suspense fallback="Loading ...">
              <Box sx={{ margin: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Devis
                </Typography>
                <QuotesList customerId={id} />
              </Box>
            </Suspense>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
}

export const CustomersList = () => {
  const [customers] = useQuery(getUserCustomers, {})

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell align="left">Nom</TableCell>
            <TableCell align="left">Prénom</TableCell>
            <TableCell align="left">Email</TableCell>
            <TableCell align="left">Téléphone</TableCell>
            <TableCell align="left">Infos</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {customers.map((item) => (
            <Item key={item.id} item={item} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
