import { MyButton } from "app/core/components/Button"
import Form from "app/core/components/Form"
import { useMutation, useQuery, useRouter } from "blitz"
import { TextField } from "mui-rff"
import getCustomerById from "../queries/getCustomerById"
import { CreateCustomerValidation } from "../mutations/createCustomer"
import updateCustomer from "../mutations/updateCustomer"
import deleteCustomer from "../mutations/deleteCustomer"
import { Radios } from "mui-rff"

export const CustomerUpdate = () => {
  const { query } = useRouter()
  const router = useRouter()
  const customer = useQuery(getCustomerById, { id: Number(query.id) })
  const [deleteCustomerMutation] = useMutation(deleteCustomer)
  const [updateCustomerMutation] = useMutation(updateCustomer)

  return (
    <>
      <Form
        initialValues={{ ...customer[0] }}
        schema={CreateCustomerValidation}
        onSubmit={async (values) => {
          try {
            await updateCustomerMutation({ ...values, id: Number(query.id) })
          } catch (error) {
            console.log(error)
          }
        }}
      >
        <TextField
          size="small"
          name="firstName"
          label="Prénom"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="lastName"
          label="Nom"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="email"
          label="Email"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="companyName"
          label="Nom de la société"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressCountry"
          label="Pays"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressLocality"
          label="Localité"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressPostal"
          label="Code postal"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="adressStreet"
          label="Rue"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="tva"
          label="Numéro de TVA"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="phone"
          label="Numéro de téléphone"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <Radios
          label="Connu par"
          name="knownBy"
          // defaultValue={"WORD_OF_MOUTH"}
          data={[
            { label: "Bouche à oreille", value: "WORD_OF_MOUTH" },
            { label: "Internet", value: "INTERNET" },
            { label: "Publicité", value: "ADVERTISING" },
            { label: "Autre", value: "OTHER" },
          ]}
        />
        <div className="w-full flex justify-between">
          <MyButton
            variant="contained"
            shade="ternary"
            button={{
              onClick: async () => {
                await deleteCustomerMutation(Number(query.id))
                await router.push("/customers")
              },
            }}
          >
            Supprimer
          </MyButton>
          <MyButton variant="contained" shade="secondary" button={{ type: "submit" }}>
            Enregistrer
          </MyButton>
        </div>
      </Form>
    </>
  )
}
