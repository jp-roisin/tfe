import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { useQuery } from "blitz"
import { Subtitle } from "app/core/components/Subtitile"
import getUserCustomers from "../queries/getUserCustomers"
import { useState } from "react"
import { Customer } from "db"
import { colors } from "app/core/styles/Constants"
import { MyButton } from "app/core/components/Button"

export const CustomersPick = ({
  nextButton,
  backButton,
  children,
}: {
  nextButton: {
    label: string
    onNextStep: (arg: Customer) => void
  }
  backButton: {
    label: string
    onClick: () => void
  }
  children: React.ReactNode
}) => {
  const [customers] = useQuery(getUserCustomers, {})
  const [customerPicked, setCustomerPicked] = useState<Customer>()

  const nextButtonHandler = () => {
    if (customerPicked) {
      nextButton.onNextStep(customerPicked)
    }
  }

  return (
    <>
      <div className="w-full flex justify-between">
        <Subtitle>Choix du client</Subtitle>
        {children}
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 600 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell align="left">Nom</TableCell>
              <TableCell align="left">Prénom</TableCell>
              <TableCell align="left">Entreprise</TableCell>
              <TableCell align="left">Commune</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {customers.map((item, i) => (
              <TableRow
                onClick={() => {
                  setCustomerPicked(item)
                }}
                key={item.id}
                sx={
                  !!customerPicked && customerPicked.id === item.id
                    ? { bgcolor: colors.primaryLter, cursor: "pointer" }
                    : { cursor: "pointer" }
                }
              >
                <TableCell component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell align="left">{item.lastName}</TableCell>
                <TableCell align="left">{item.firstName}</TableCell>
                <TableCell align="left">{item.companyName}</TableCell>
                <TableCell align="left">{item.adressLocality}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className="w-full flex justify-end mt-8">
        <MyButton button={{ onClick: nextButtonHandler }} variant="contained" shade="primary">
          {nextButton.label}
        </MyButton>
      </div>
    </>
  )
}
