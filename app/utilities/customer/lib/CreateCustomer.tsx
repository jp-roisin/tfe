import Form, { FormProps } from "app/core/components/Form"
import { TextField } from "mui-rff"
import { CreateCustomerValidation } from "app/utilities/customer/mutations/createCustomer"
import { Button } from "@mui/material"
import React from "react"
import { Subtitle } from "app/core/components/Subtitile"
import { MyButton } from "app/core/components/Button"
import { Radios } from "mui-rff"

export const CreateCustomer = ({
  submitText,
  onSubmit,
  initialValues,
  backButton,
  children,
}: {
  submitText: string
  onSubmit: FormProps<typeof CreateCustomerValidation>["onSubmit"]
  initialValues?: FormProps<typeof CreateCustomerValidation>["initialValues"]
  backButton?: Omit<React.ComponentProps<typeof Button>, "variant" | "color">
  children: React.ReactNode
}) => {
  return (
    <>
      <div className="my-6 w-full flex items-center justify-between">
        <Subtitle>Nouveau client</Subtitle>
        {children}
      </div>
      <Form initialValues={initialValues} schema={CreateCustomerValidation} onSubmit={onSubmit}>
        <div>
          <Radios
            label="Type de client"
            name="customerType"
            required={true}
            defaultValue={"PERSON"}
            data={[
              { label: "Particulier", value: "PERSON" },
              { label: "Société", value: "COMPANY" },
              { label: "Syndicat ou ACP", value: "SYNDICATE" },
              { label: "Autre", value: "OTHER", disabled: true },
            ]}
          />
        </div>

        <TextField
          required={true}
          size="small"
          name="firstName"
          label="Prénom"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="lastName"
          label="Nom"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="email"
          label="Email"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="phone"
          label="Numéro de téléphone"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="adressCountry"
          label="Pays"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="adressLocality"
          label="Localité"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="adressPostal"
          label="Code postal"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          required={true}
          size="small"
          name="adressStreet"
          label="Rue"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="tva"
          label="Numéro de TVA"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <TextField
          size="small"
          name="companyName"
          label="Nom de la société"
          variant="standard"
          margin="dense"
          fullWidth
        />
        <Radios
          label="Connu par"
          name="knownBy"
          defaultValue={"WORD_OF_MOUTH"}
          data={[
            { label: "Bouche à oreille", value: "WORD_OF_MOUTH" },
            { label: "Internet", value: "INTERNET" },
            { label: "Publicité", value: "ADVERTISING" },
            { label: "Autre", value: "OTHER" },
          ]}
        />
        <div className="w-full flex justify-end">
          <MyButton variant="contained" shade="primary" button={{ type: "submit" }}>
            {submitText}
          </MyButton>
        </div>
      </Form>
    </>
  )
}
