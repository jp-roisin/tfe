import { resolver } from "blitz"
import db from "db"
import * as z from "zod"
import { CreateCustomerValidation } from "./createCustomer"

export const UpdateCustomerValidation = CreateCustomerValidation.extend({
  id: z.number(),
})

export default resolver.pipe(resolver.zod(UpdateCustomerValidation), resolver.authorize(), (data) =>
  db.customer.update({
    where: { id: data.id },
    data,
  })
)
