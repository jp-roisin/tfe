import { Ctx, resolver } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateCustomerValidation = z.object({
  firstName: z.string().nonempty(),
  lastName: z.string().nonempty(),
  customerType: z.enum(["PERSON", "COMPANY", "SYNDICATE"]),
  companyName: z.string().nullable(),
  adressCountry: z.string().nonempty(),
  adressLocality: z.string().nonempty(),
  adressPostal: z.string().nonempty(),
  adressStreet: z.string().nonempty(),
  tva: z.string().nullable(),
  phone: z.string().nonempty(),
  email: z.string().email().nonempty(),
  knownBy: z.enum(["WORD_OF_MOUTH", "INTERNET", "ADVERTISING", "OTHER"]),
})

export default resolver.pipe(
  resolver.zod(CreateCustomerValidation),
  resolver.authorize(),
  (data, ctx: Ctx) =>
    db.customer.create({
      data: { ...data, userId: ctx.session.userId },
    })
)
