import db from "db"
import { AuthorizationError, Ctx } from "blitz"

import * as z from "zod"

const CustomerIdValidation = z.object({
  id: z.number(),
})

export default async function getCustomerById(
  input: z.infer<typeof CustomerIdValidation>,
  ctx: Ctx
) {
  ctx.session.$authorize()
  const data = CustomerIdValidation.parse(input)
  const customer = await db.customer.findFirst({ where: { id: data.id, deleted: false } })

  if (customer?.userId !== ctx.session.userId) {
    throw new AuthorizationError()
  }

  return customer
}
