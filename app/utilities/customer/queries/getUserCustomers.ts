import db from "db"
import { Ctx } from "blitz"

import * as z from "zod"

const InputValidation = z.object({})

export default async function getUserCustomers(input: z.infer<typeof InputValidation>, ctx: Ctx) {
  ctx.session.$authorize()
  return await db.customer.findMany({ where: { userId: ctx.session.userId, deleted: false } })
}
