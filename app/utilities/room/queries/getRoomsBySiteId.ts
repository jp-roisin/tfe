import db from "db"
import { Ctx } from "blitz"
import * as z from "zod"

const getRoomsBySiteIdValidation = z.object({
  siteId: z.number(),
})

export default async function getRoomsBySiteId(
  input: z.infer<typeof getRoomsBySiteIdValidation>,
  ctx: Ctx
) {
  ctx.session.$authorize()
  return await db.electricRoom.findMany({ where: { siteId: input.siteId, deleted: false } })
}
