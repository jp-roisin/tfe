import { resolver } from "blitz"
import db from "db"
import * as z from "zod"

export const CreateRoomValidation = z.object({
  siteId: z.number(),
  name: z.string().nonempty(),
})

export default resolver.pipe(resolver.zod(CreateRoomValidation), resolver.authorize(), (data) =>
  db.electricRoom.create({
    data: { ...data },
  })
)
