import { resolver } from "blitz"
import db from "db"
import * as z from "zod"

export const AddElectricItemsToRoomValidation = z.object({
  id: z.number(),
  electricItemIds: z.array(z.number()),
})

export default resolver.pipe(
  resolver.zod(AddElectricItemsToRoomValidation),
  resolver.authorize(),
  (data) =>
    db.electricRoom.update({
      where: { id: data.id },
      data: {
        elecricItems: {
          connect: data.electricItemIds.map((id) => ({ id })),
        },
      },
    })
)
