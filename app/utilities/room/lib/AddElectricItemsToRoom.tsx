import { useState } from "react"
import getUserElectricItems from "app/utilities/electricItem/queries/getUserElectricItems"

import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { MyButton } from "app/core/components/Button"
import { useMutation, useQuery } from "blitz"
import { ElectricItem } from "db"
import addElectricItemsToRoom from "../mutations/addElectricItemsToRoom"
import { Subtitle } from "app/core/components/Subtitile"

const Row = ({
  item,
  countUpHandler,
  countDownHandler,
}: {
  item: ElectricItem
  countUpHandler: (item: ElectricItem) => void
  countDownHandler: (item: ElectricItem) => void
}) => {
  const [count, setCount] = useState<number>(0)

  const onCountUp = () => {
    setCount((prev) => prev + 1)
    countUpHandler(item)
  }

  const onCountDown = () => {
    if (count > 0) {
      setCount((prev) => prev - 1)
      countDownHandler(item)
    }
  }

  return (
    <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
      <TableCell align="left">{item.name}</TableCell>
      <TableCell align="left">{`${item.price} €`}</TableCell>
      <TableCell align="left">
        <MyButton
          variant="contained"
          shade="primaryLt"
          button={{
            onClick: () => onCountDown(),
          }}
        >
          -
        </MyButton>
      </TableCell>
      <TableCell align="left">{count}</TableCell>
      <TableCell align="left">
        <MyButton
          variant="contained"
          shade="primaryLt"
          button={{
            onClick: () => onCountUp(),
          }}
        >
          +
        </MyButton>
      </TableCell>
    </TableRow>
  )
}

export const AddElectricItemsToRoom = ({
  roomId,
  roomName,
}: {
  roomId: number
  roomName: string
}) => {
  const [electricItemsArray, setElectricItemsArray] = useState<ElectricItem[]>([])
  const [userElectricItems] = useQuery(getUserElectricItems, {})
  const [updateRoomMutation] = useMutation(addElectricItemsToRoom)

  const addElectricItemHandler = (item: ElectricItem) => {
    setElectricItemsArray((prev) => [...prev, item])
  }

  const removeElectricItemHandler = (item: ElectricItem) => {
    const index = electricItemsArray.findIndex((element) => element === item)
    setElectricItemsArray((prev) => {
      prev.splice(index, 1)
      return [...prev]
    })
  }

  const onSaveElectricItemsArray = async () => {
    try {
      await updateRoomMutation({
        id: roomId,
        electricItemIds: electricItemsArray.map((item) => item.id),
      })
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <Subtitle>{roomName}</Subtitle>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 300 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Nom</TableCell>
              <TableCell align="left">Prix hTVA</TableCell>
              <TableCell align="left"></TableCell>
              <TableCell align="left">Compte</TableCell>
              <TableCell align="left"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userElectricItems.map((item) => (
              <Row
                key={item.id}
                item={item}
                countUpHandler={addElectricItemHandler}
                countDownHandler={removeElectricItemHandler}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className="my-4 w-full flex justify-end">
        <MyButton
          variant="contained"
          shade="secondary"
          button={{ onClick: () => onSaveElectricItemsArray() }}
        >
          Enregistrer
        </MyButton>
      </div>
    </>
  )
}
