import { Suspense, useState } from "react"
import { useMutation, useQuery } from "blitz"
import getRoomsBySiteId from "../queries/getRoomsBySiteId"
import { CreateRoom } from "./CreateRoom"

import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import { Modal } from "app/core/components/Modal"
import { MyButton } from "app/core/components/Button"
import { AddElectricItemsToRoom } from "./AddElectricItemsToRoom"
import deleteRoom from "../mutations/deleteRoom"

const initialValues = {
  isOpen: false,
  roomId: 0,
  roomName: "",
}

export const ListRooms = ({ siteId }: { siteId: number }) => {
  const [modalState, setModalState] = useState(initialValues)
  const [rooms, extras] = useQuery(getRoomsBySiteId, { siteId })
  const [deleteRoomMutation] = useMutation(deleteRoom)

  const refreshListHandler = () => {
    extras.refetch()
  }

  const modalHandler = () => {
    setModalState((prev) => ({ ...prev, isOpen: !prev.isOpen }))
  }

  const modalIdHandler = (roomId: number, roomName: string) => {
    setModalState((prev) => ({ ...prev, roomId, roomName }))
  }

  const onDeleteEletricRoom = async (roomId: number) => {
    try {
      await deleteRoomMutation(roomId)
      await extras.refetch()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      {!!modalState.isOpen && (
        <Modal onCloseModal={modalHandler}>
          <Suspense fallback="Loading...">
            <div className="m-8">
              <AddElectricItemsToRoom roomId={modalState.roomId} roomName={modalState.roomName} />
            </div>
          </Suspense>
        </Modal>
      )}
      <CreateRoom siteId={siteId} refresh={refreshListHandler} />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 600 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Nom</TableCell>
              <TableCell align="left">Editer</TableCell>
              <TableCell align="left">Supprimer</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rooms.map(({ id, name }) => (
              <TableRow key={id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <TableCell align="left">{name}</TableCell>
                <TableCell align="left">
                  <MyButton
                    variant="contained"
                    shade="primaryLt"
                    button={{
                      onClick: () => {
                        modalIdHandler(id, name)
                        modalHandler()
                      },
                    }}
                  >
                    Edit
                  </MyButton>
                </TableCell>
                <TableCell align="left">
                  <MyButton
                    variant="contained"
                    shade="ternary"
                    button={{
                      onClick: () => onDeleteEletricRoom(id),
                    }}
                  >
                    X
                  </MyButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
