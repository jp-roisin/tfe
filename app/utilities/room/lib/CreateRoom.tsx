import { MyButton } from "app/core/components/Button"
import Form from "app/core/components/Form"
import { TextField } from "mui-rff"
import createRoom, { CreateRoomValidation } from "../mutations/createRoom"
import { useMutation } from "blitz"

export const CreateRoom = ({ siteId, refresh }: { siteId: number; refresh: () => void }) => {
  const [createRoomMutation] = useMutation(createRoom)

  return (
    <>
      <Form
        initialValues={{ name: "Pièce 1" }}
        schema={CreateRoomValidation}
        onSubmit={async (values) => {
          try {
            await createRoomMutation({ ...values, siteId })
            await refresh()
          } catch (error) {
            console.log({ ...values, siteId })
            console.log(error)
          }
        }}
      >
        <TextField
          name="name"
          label="Nom de pièce"
          variant="standard"
          margin="normal"
          required
          fullWidth
        />
        <MyButton variant="contained" shade="primary" button={{ type: "submit" }}>
          Ajouter
        </MyButton>
      </Form>
    </>
  )
}
