import { BlitzLayout } from "blitz"

import { ComponentProps } from "react"
import { LayoutBase } from "./LayoutBase"

export const LayoutHome: BlitzLayout<ComponentProps<typeof LayoutBase>> = ({
  children,
  ...layoutBase
}) => {
  return (
    <LayoutBase {...layoutBase}>
      <div className="flex justify-center items-center h-[100vh] w-full bg-primary">{children}</div>
    </LayoutBase>
  )
}
