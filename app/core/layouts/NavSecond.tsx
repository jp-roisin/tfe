import { Link, useRouter } from "blitz"
import { Suspense } from "react"

export const NavSecond = ({ children }: { children: JSX.Element }) => {
  const { route, query } = useRouter()
  const customerId = query.id

  const accountLinks = [
    { label: "Compte", link: "/account", onActive: "/account" },
    { label: "Catalogue", link: "/account/catalog", onActive: "/account/catalog" },
    { label: "Settings", link: "/account/settings", onActive: "/account/settings" },
    { label: "Admin", link: "/account/admin", onActive: "/account/admin" },
  ]

  const customerLinks = [
    { label: "Client", link: `/customers/${customerId}`, onActive: "/customers/[id]" },
    {
      label: "Chantiers",
      link: `/customers/${customerId}/sites`,
      onActive: "/customers/[id]/sites",
    },
    { label: "Devis", link: `/customers/${customerId}/quotes`, onActive: "/customers/[id]/quotes" },
    {
      label: "Factures",
      link: `/customers/${customerId}/invoices`,
      onActive: "/customers/[id]/invoices",
    },
  ]

  const mappedItems = route.startsWith("/account") ? accountLinks : customerLinks
  return (
    <>
      <ul className="flex justify-center my-12 text-lg">
        {mappedItems.map((item, i) => (
          <li key={i} className="mx-6">
            <Link href={item.link}>
              <a
                className={
                  route === item.onActive
                    ? "text-primary border-b-2 border-primary pb-2"
                    : "text-primaryLt"
                }
              >
                {item.label}
              </a>
            </Link>
          </li>
        ))}
      </ul>
      <div>
        <Suspense fallback="Loading...">{children}</Suspense>
      </div>
    </>
  )
}
