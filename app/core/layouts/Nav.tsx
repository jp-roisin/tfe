import { Image, Link, useRouter } from "blitz"
import HomeIcon from "../assets/basil/HomeIcon"
import AccountIcon from "../assets/basil/AccountIcon-28"
import ListingIcon from "../assets/basil/ListingIcon"
import QuoteIcon from "../assets/basil/QuoteIcon"
import { colors } from "../styles/Constants"
import classNames from "classnames"
import React from "react"
import logo from "../assets/logo/logo.svg"

const Item = ({ navItem }: { navItem: { active: boolean; link: string; label: string } }) => {
  const { active, link, label } = navItem
  const fillColor = active ? colors.secondary : colors.primaryLt
  let icon
  switch (link) {
    case "/home":
      icon = <HomeIcon fill={fillColor} />
      break
    case "/customers":
      icon = <ListingIcon fill={fillColor} />
      break
    case "/quotes":
      icon = <QuoteIcon fill={fillColor} />
      break
    case "/account":
      icon = <AccountIcon fill={fillColor} />
      break
    case "/test":
      icon = <AccountIcon fill={fillColor} />
      break
    default:
      icon = <></>
      break
  }
  return (
    <Link href={link}>
      <a>
        <div className="text-lg ml-6 flex items-center">
          <div className="mr-4">{icon}</div>
          <p className={classNames("text-2xl", active ? "text-secondary" : "text-primaryLt")}>
            {label}
          </p>
        </div>
      </a>
    </Link>
  )
}

export const Nav = ({ classes }: { classes: string }) => {
  const { pathname } = useRouter()
  const navItems = [
    {
      label: "Home",
      link: "/home",
      active: pathname === "/home",
    },
    {
      label: "Listing",
      link: "/customers",
      active: pathname.startsWith("/customers"),
    },
    {
      label: "Devis",
      link: "/quotes",
      active: pathname === "/quotes",
    },
    {
      label: "Compte",
      link: "/account",
      active: pathname.startsWith("/account"),
    },
    {
      label: "Test",
      link: "/test",
      active: pathname.startsWith("/account"),
    },
  ]

  return (
    <div className={`w-[230px] min-h-[100vh] bg-primary p-4 ${classes}`}>
      <div className="flex flex-col items-center mb-16">
        <Image src={logo} alt="logo" width="128px" />
        <p className="text-white italic text-lg">{"Travail de fin d'étude"}</p>
      </div>
      <ul>
        {navItems.map((item, i) => (
          <li
            key={i}
            className="py-2 cursor-pointer border-b-2 border-subPrimary hover:text-secondary"
          >
            <Item navItem={item} />
          </li>
        ))}
      </ul>
    </div>
  )
}
