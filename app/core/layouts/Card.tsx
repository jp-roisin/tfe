export const Card: React.FC<{ classes: string }> = ({ classes, children }) => {
  // const style = `rounded-lg m-4 p-4 bg-ternaryLght ${classes}`

  return <div className={`rounded-lg m-4 p-12 ${classes}`}>{children}</div>
}
