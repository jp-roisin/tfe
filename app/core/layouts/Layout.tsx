import { BlitzLayout } from "blitz"

import { ComponentProps } from "react"
import { LayoutBase } from "./LayoutBase"
import { Nav } from "./Nav"

export const Layout: BlitzLayout<ComponentProps<typeof LayoutBase>> = ({
  children,
  ...layoutBase
}) => {
  return (
    <LayoutBase {...layoutBase}>
      <div className="flex ">
        <Nav classes="flex-none" />
        <div className="flex-1">
          <div className="px-10">{children}</div>
        </div>
      </div>
    </LayoutBase>
  )
}
