export const Subtitle = ({ children }: { children: string }) => (
  <div className="text-xl italic text-primary my-8 bg-primaryLter w-fit rounded-2xl px-4 py-2">
    {children}
  </div>
)
