import React from "react"

export const Modal = ({
  onCloseModal,
  children,
}: {
  onCloseModal: () => void
  children: React.ReactNode | string
}) => {
  return (
    <>
      <div
        className="top-0 left-0 w-[100%] h-[100vh] z-10 bg-black/[0.7] absolute"
        onClick={onCloseModal}
      />
      <div className="absolute z-20 rounded-xl top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] w-[70%] h-[600px] overflow-hidden bg-white">
        {children}
      </div>
    </>
  )
}
