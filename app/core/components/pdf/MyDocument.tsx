import React from "react"
import { Page, Text, View, Document, StyleSheet } from "@react-pdf/renderer"
import { QuoteForm } from "app/utilities/quote/lib/QuoteStepper"
import { useQuery } from "blitz"
import getUserData from "app/utilities/user/queries/getUserData"
import { User } from "db"

const styles = StyleSheet.create({
  page: {
    flexDirection: "column",
    backgroundColor: "#a9dbead7d",
    width: "100%",
  },
  userInfo: {
    alignItems: "flex-end",
    flexGrow: 1,
    fontSize: 22,
  },
  customerInfo: {
    alignItems: "flex-start",
    fontSize: 22,
  },
})

export const MyDocument = ({ info, user }: { info: QuoteForm; user: User | null }) => {
  return (
    <>
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.userInfo}>
            <Text>{`${user?.adressStreet || ""}, ${user?.adressPostal || ""} ${
              user?.adressLocality || ""
            }, ${user?.adressCountry || ""}`}</Text>
          </View>
          <View style={styles.customerInfo}>
            <Text>{info.customer?.firstName || ""}</Text>
          </View>
        </Page>
      </Document>
    </>
  )
}
