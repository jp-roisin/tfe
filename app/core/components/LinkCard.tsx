import { Link } from "blitz"
import { ReactNode } from "react"

export const LinkCard = ({ href, label }: { href: string; label: string | ReactNode }) => {
  return (
    <div className="w-[30%] h-[125px] bg-white rounded-xl flex justify-center items-center text-center my-4 cursor-pointer text-xl">
      <Link href={href}>
        <a>{label}</a>
      </Link>
    </div>
  )
}
