import { Button } from "@mui/material"
import React from "react"
import { colors } from "app/core/styles/Constants"

export const MyButton = ({
  shade,
  variant,
  button,
  children,
}: {
  shade: "primary" | "primaryLt" | "secondary" | "ternary"
  variant: "contained" | "outlined"
  children: React.ReactNode
  button?: Omit<React.ComponentProps<typeof Button>, "variant" | "color" | "children">
}) => {
  let backgroundColor
  let color
  let borderColor
  switch (shade) {
    case "primary":
      backgroundColor = variant === "contained" ? colors.primary : colors.white
      color = variant === "contained" ? colors.white : colors.primary
      borderColor = colors.primary
      break
    case "primaryLt":
      backgroundColor = variant === "contained" ? colors.primaryLter : colors.primaryLter
      color = variant === "contained" ? colors.primary : colors.primary
      borderColor = colors.primary
      break
      break
    case "secondary":
      backgroundColor = variant === "contained" ? colors.secondary : colors.white
      color = variant === "contained" ? colors.white : colors.secondary
      borderColor = colors.secondary
      break
    case "ternary":
      backgroundColor = variant === "contained" ? colors.ternary : colors.white
      color = variant === "contained" ? colors.white : colors.ternary
      borderColor = colors.ternary
      break
    default:
      backgroundColor = variant === "contained" ? colors.primary : colors.primaryLter
      color = variant === "contained" ? colors.white : colors.primary
      borderColor = colors.primary
      break
  }
  return (
    <Button
      {...button}
      variant={variant}
      sx={{
        backgroundColor,
        color,
        borderColor,
        textTransform: "inherit",
        fontWeight: "bold",
        "&:hover": { backgroundColor, borderColor },
      }}
    >
      {children}
    </Button>
  )
}
