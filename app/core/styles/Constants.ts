import tailwind from "../../../tailwind.config"

export const fontsFamily = {}

export type Colors = keyof typeof colors

export const colors = tailwind.theme.extend.colors
export const screens = tailwind.theme.extend.screens
