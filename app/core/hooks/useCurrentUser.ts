import { useQuery } from "blitz"
import getCurrentUser from "app/utilities/user/queries/getCurrentUser"

export const useCurrentUser = () => {
  const [user] = useQuery(getCurrentUser, null)
  return user
}
