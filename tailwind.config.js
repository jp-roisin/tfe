// tailwind.config.js
module.exports = {
  content: ["{pages,app}/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      screens: {
        xs: "550px",
        sm: "768px",
        md: "960px",
        lg: "1050px",
        xl: "1180px",
        xxl: "1300px",
      },
      colors: {
        white: "#fff",
        black: "#000",
        primary: "#1F2C36",
        subPrimary: "#2A3642",
        primaryLt: "#b1c8de",
        primaryLter: "#ceddeb",
        secondary: "#ECB22E",
        ternary: "#E01E5A",
        gray: "#8B8B8B",
        success: "#1B5E20",
        successLt: "#b0e8b4",
        error: "#D32F2F",
        errorLt: "#edbebe",
      },
    },
  },
  plugins: [],
}
